# 3DCP

This reposotory contains the mechanical design of a 1x1x1 concrete 3D printer. ALternative solutions and design decisions are explained. A delta configuration is selected. An accuracy study is conducted, along with the creation of an accuracy map of the mechanism.
Moreover, a Simscape simulation is created in order to study the dynamic characteristics of the system. ALl the needed components are calculated. The CAD is presented as a .step and .f3d file as it is done in Fusion 360. 
