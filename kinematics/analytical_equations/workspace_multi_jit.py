import numpy as np 
import time 
import multiprocessing 
from numba import jit


@jit(nopython = True)
def fkp_pos_alt(l_1,l_2,l_3):
    l = 1.49650 # arm length -- current design
    l = 1.60 # increased arm length to reach all spots
    SB = 1.626695
    sP = 0.147224
    WB = np.sqrt(3)/6 * SB 
    UB = np.sqrt(3)/3 * SB 
    wP = np.sqrt(3)/6 * sP 
    uP = np.sqrt(3)/3 * sP 
    bed_radius = WB 

    a = SB/2 - sP/2 
    b = WB - wP 
    c = uP - UB


    d = (l_2 - l_1)/2/a 
    e = (l_2**2 - l_1**2)/4/a
    D = (l_3 - l_1 - a*d)/(b - c)
    E = (c**2 - a**2 -b**2 -2*a*e + l_3**2 - l_1**2)/2/(b - c)

    A = d**2 + D**2 + 1 
    B = 2*(d*e + D*E + c*D +l_3)
    C = e**2 + E**2 + c**2 + 2*c*E + l_3**2 -l**2

    z = (-B + np.sqrt(B**2 - 4*A*C))/2/A
    # if(abs(z)>l_1 or abs(z)> l_2 or abs(z)>l_3): 
    if(z>0):
        z = (-B - np.sqrt(B**2 - 4*A*C))/2/A
    
    x = d*z + e 
    y = D*z + E

    x = np.float32(x)
    y = np.float32(y)
    z = np.float32(z)

    return [x,y,z]

@jit(nopython = True)
def solve_region(region_steps,region_start,region_end,number): 
    # printer data
    Lmin = np.float32(0.005) # for the full printing range
    Lmax = np.float32(2.4)# for the full printing range
    # Lmin = np.float32(0.26865)
    # Lmax = np.float32(1.72211) 
    L_steps = 850
    running_range = np.arange(L_steps)
    L2 = np.linspace(Lmin,Lmax,L_steps) 
    L3 = np.linspace(Lmin,Lmax,L_steps) 
    region_points = np.zeros(shape = (region_steps*L_steps**2,3),dtype=np.float32)
    region_L1_points = np.linspace(region_start,region_end,region_steps)
    point_counter = 0 
    for i in region_L1_points: 
        for j in running_range: 
            for k in running_range: 
                # point_x,point_y,point_z = fsolve(equations,(0,0, -np.average([i,L2[j],L3[k]])-l),args = (i,L2[j],L3[k]))
                # point_x, point_y, point_z = fkp_pos(i,L2[j],L3[k],0,0, -np.average([i,L2[j],L3[k]])-l)
                point = fkp_pos_alt(i,L2[j],L3[k])
                # if(point[0] == -20): 
                #     continue  
                region_points[point_counter,0] = point[0]
                region_points[point_counter,1] = point[1]
                region_points[point_counter,2] = point[2]
                point_counter = point_counter + 1 
                
    return region_points

def solve_region_wrap(region_steps,region_start,region_end,number): 
    # Just a wrapper function for solve_region
    region_points = solve_region(region_steps,region_start,region_end,number)
    np.savetxt('kinematics/analytical_equations/results/fullrange_n850/worksp_region%d.csv'%number,region_points,fmt = "%.4lf",delimiter=',')
    return 


if __name__ == '__main__' : 

    time_start = time.time()
    L_steps = 850
    # L_steps = 480
    Lmin = 0.005 # for the full printing range
    Lmax = 2.4 # for the full printing range
    # Lmin = np.float32(0.26865)
    # Lmax = np.float32(1.72211) 

    NPROCESSES = 10
    region_distance = (Lmax - Lmin)/NPROCESSES
    region_steps = 85 
    processes = []
    
    for i in range(NPROCESSES): 
        p = multiprocessing.Process(target = solve_region_wrap, args = (region_steps,Lmin + i*region_distance,Lmin+(i+1)*region_distance,i+1))
        p.start()
        processes.append(p)
        print("Started process : %d"%i)

    # p1 = multiprocessing.Process(target = solve_region, args = (region_steps,Lmin,Lmin+region_distance,1))
    # p2 = multiprocessing.Process(target = solve_region, args = (region_steps,Lmin+region_distance,Lmin+2*region_distance,2,))
    # p3 = multiprocessing.Process(target = solve_region, args = (region_steps,Lmin+2*region_distance,Lmin+3*region_distance,3,))
    # p4 = multiprocessing.Process(target = solve_region, args = (region_steps,Lmin+3*region_distance,Lmin+4*region_distance,4,))
    # p5 = multiprocessing.Process(target = solve_region, args = (region_steps,Lmin+4*region_distance,Lmin+5*region_distance,5,))
    # p6 = multiprocessing.Process(target = solve_region, args = (region_steps,Lmin+5*region_distance,Lmax,6,))

    # p1.start()
    # p2.start()
    # p3.start()
    # p4.start()
    # p5.start()
    # p6.start()

    # processes = [p1,p2,p3,p4,p5,p6]
    # processes = [p1]
    for process in processes : 
        process.join()

    time_end = time.time()
    print("Elapsed time : %lf sec"%(time_end - time_start))
