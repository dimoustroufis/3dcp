import numpy as np 
import matplotlib.pyplot as plt 
from tqdm import tqdm
from delta import Delta 
import time 

import multiprocessing

# Printer geometry data and vertical print level 
l = 1.49650 # arm length 
SB = 1.626695
sP = 0.147224

# l = 0.264 # from a real design 
# SB = 0.246 # from a real design 
# sP = 0.127 # from a real design 

WB = np.sqrt(3)/6 * SB 
UB = np.sqrt(3)/3 * SB 
wP = np.sqrt(3)/6 * sP 
uP = np.sqrt(3)/3 * sP 

rail_height = 2.710 # is  not used at this point 
bed_radius = 1.430/2 # is  not used at this point 
printhead_radius = 0.085 # is  not used at this point 

bed_radius = WB 

printer = Delta(l,SB,sP,WB,wP,uP,UB,rail_height, bed_radius,printhead_radius)


def planar_dist_sq(x1,y1,x2=0,y2=0): 
    # squared to reduce the computational cost of calculating sqrt()
    return (x1-x2)**2 + (y1-y2)**2


# Full motion range
Lmin = 0.005 # for the full printing range
Lmax = 2.3 # for the full printing range
# Lmin = 0.067 # from a real design 
# Lmax = 0.358 # from a real design  
# L_steps = 1248
L_steps = 850
L1 = np.linspace(Lmin,Lmax,L_steps) 
L2 = np.linspace(Lmin,Lmax,L_steps) 
L3 = np.linspace(Lmin,Lmax,L_steps) 

def solve_region(region_steps,region_start,region_end,number): 
    region_points = np.zeros(shape = (3,region_steps*L_steps**2))
    region_L1_points = np.linspace(region_start,region_end,region_steps)
    point_counter = 0 
    for i in tqdm(region_L1_points): 
        for j in range(L_steps): 
            for k in range(L_steps): 
                point = printer.fkp_pos(i,L2[j],L3[k],0,0, -np.average([i,L2[j],L3[k]])-l)
                # point = printer.fkp_pos(i,L2[j],L3[k],0,0, -np.average([i,L2[j],L3[k]])-0.2) # from a real design 
                region_points[0,point_counter] = point[0]
                region_points[1,point_counter] = point[1]
                region_points[2,point_counter] = point[2]
                point_counter = point_counter + 1 
    
    np.savetxt('kinematics/analytical_equations/results/1_region%d.csv'%number,region_points,fmt = "%.4lf",delimiter=',')
    return region_points
    

if __name__ == '__main__' : 

    time_start = time.time()

    NPROCESSES = 1
    region_distance = (Lmax - Lmin )/NPROCESSES 
    # region_points1 = list(np.zeros(shape = (3,50**3)))
    # region_points1 = np.zeros(shape = (3,50**3))

    # region_steps = int(1248/6)
    region_steps = 850
    processes = []
    for i in range(NPROCESSES): 
        p = multiprocessing.Process(target = solve_region, args = (region_steps,Lmin + i*region_distance,Lmin+(i+1)*region_distance,i+1))
        p.start()
        processes.append(p)
        print("Started process : %d"%i)

    time_end = time.time()
    print("Elapsed time : %lf sec"%(time_end - time_start))
