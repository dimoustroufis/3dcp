# Some code to calculate the minimum required actuator resolution in order to achieve the required accuracy
from delta import Delta 
import numpy as np 
import matplotlib.pyplot as plt


# Printer geometry data and vertical print level 
# l = 1.49650 # arm length 
l = 1.55
SB = 1.626695
sP = 0.147224
# WB = 0.816909
# wP = 0.045069
# uP = 0.084999
# UB = 0.900000

WB = np.sqrt(3)/6 * SB 
UB = np.sqrt(3)/3 * SB 
wP = np.sqrt(3)/6 * sP 
uP = np.sqrt(3)/3 * sP 

rail_height = 2.710 
bed_radius = 1.430/2 
printhead_radius = 0.085 

z = -1.8

# Actuator position timeseries export 
printer = Delta(l,SB,sP,WB,wP,uP,UB,rail_height,bed_radius,printhead_radius)


def rotate_point(x_point,y_point,theta): 
    # theta in degrees 
    theta = np.deg2rad(theta)
    x_orig = x_point
    y_orig = y_point
    x_point = x_orig*np.cos(theta) -y_orig*np.sin(theta)
    y_point = y_orig*np.cos(theta) + x_orig*np.sin(theta) 
    
    return x_point,y_point

dL1 = [] 
dL2 = []
dL3 = []
destination_x = 0.005
destination_y = 0
x_points = [0.005]
y_points = [0]
L_start = printer.ikp_pos(0,0,z)
L1_end = []
L2_end = []
L3_end = []

theta = np.linspace(0,90,360)
for i in range(len(theta)): 
    destination_x, destination_y = rotate_point(destination_x,destination_y, theta[i])
    x_points.append(destination_x)
    y_points.append(destination_y)       
    res = printer.ikp_pos(destination_x, destination_y,z)
    L1_end.append(res[0])
    L2_end.append(res[1])
    L3_end.append(res[2])

    dL1.append(abs(L_start[0] - L1_end[i]))
    dL2.append(abs(L_start[1] - L2_end[i]))
    dL3.append(abs(L_start[2] - L3_end[i]))

 
print(min(dL1))
print(min(dL2))
print(min(dL3))

plt.scatter(x_points,y_points)
plt.show()