import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
from time import time 
from tqdm import tqdm 
import numpy as np


def rotate_curve(x_points,y_points,theta): 
    # theta in degrees 
    theta = np.deg2rad(theta)
    for i in range(len(x_points)): 
        x_orig = x_points[i]
        y_orig = y_points[i]
        x_points[i] = x_orig*np.cos(theta) -y_orig*np.sin(theta)
        y_points[i] = y_orig*np.cos(theta) + x_orig*np.sin(theta) 
    
    return x_points,y_points


print("Starting loading")
load_time_start = time()

NPROCESSES = 8

filename = "kinematics/analytical_equations/results/fullrange_n850/coplanar_points.csv"
file = pd.read_csv(filename, engine = "pyarrow",header=None)
file.astype('float32')
coplanar_points = np.array(file)

filename = "kinematics/analytical_equations/results/fullrange_n850/neighbors_region1.csv"
file = pd.read_csv(filename, engine = "pyarrow",header=None)
file.astype('float32')
neighbors = np.array(file)


for i in tqdm(range(1,NPROCESSES)):    
    filename = "kinematics/analytical_equations/results/fullrange_n850/neighbors_region%d.csv"%(i+1)
    file = pd.read_csv(filename, engine = "pyarrow",header=None)
    file.astype('float32')
    file = np.array(file)
    neighbors = np.append(neighbors,file)

load_time_end = time()
print("Loading time = %lf"%(load_time_end-load_time_start))


l = 1.49650 # arm length 
SB = 1.626695
sP = 0.147224

WB = np.sqrt(3)/6 * SB 
UB = np.sqrt(3)/3 * SB 
wP = np.sqrt(3)/6 * sP 
uP = np.sqrt(3)/3 * sP 

rail_height = 2.710 
bed_radius = WB
printhead_radius = 0.085 

print("The minimum number of neighbors is : %lf"%(min(neighbors)))

# ============================== PLOTTING  ================================

# Accuracy heatmap plot
fig, ax = plt.subplots()
# due to rounding in the multiprocessing task breakdown, a list is some (1-10) points bigger than the other
coplanar2neigh_diff = len(neighbors) - len(coplanar_points) 
colors = [0 for _ in range(len(coplanar_points)+coplanar2neigh_diff)]

for i in range(len(coplanar_points)+coplanar2neigh_diff): 
    if(coplanar_points[i,2] == 1): 
        colors[i] = 'green'
    if(coplanar_points[i,2] == -1): 
        colors[i] = 'red'

if coplanar2neigh_diff != 0 : 
    p = ax.scatter(coplanar_points[:coplanar2neigh_diff,0],coplanar_points[:coplanar2neigh_diff,1],c=neighbors, cmap = "jet", vmin=2, vmax=40, s=0.5)
else: 
    p = ax.scatter(coplanar_points[:,0],coplanar_points[:,1],c=neighbors, cmap = "jet", vmin=2, vmax=40, s= 0.5)

ax.set_title("Point density")
ax.set_xlabel("x [m]")
ax.set_ylabel("y [m]")
ax.grid(linestyle = "--", alpha = 0.5)
ax.set_xlim([-1,1])
ax.set_ylim([-1,1])


x_towers = [-SB/2,SB/2,0,-SB/2] # fourth point just to close the triangle on the plot
y_towers = [-WB,-WB,UB,-WB] # fourth point just to close the triangle on the plot
plt.plot(x_towers,y_towers,c="k",linestyle = "--")
plt.show()