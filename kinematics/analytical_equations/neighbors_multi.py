import numpy as np 
from tqdm import tqdm
from time import time
import pandas as pd 

import multiprocessing

from numba import jit 

@jit(nopython= True)
def find_neighbors(region_start,region_end,coplanar_points,n_coplanar_points,number):
    search_r = 0.005 # 0.005 is the needed accuracy of the printer, and we square it to avoid root calculations 
    # n_coplanar_points = len(coplanar_points[0])
    region_points = np.zeros(shape = (region_end - region_start,2))

    # assign the desired coplanar_points to region_points 
    for i in range(len(region_points)):
        region_points[i,0] = coplanar_points[region_start+i][0]
        region_points[i,1] = coplanar_points[region_start+i][1]

    region_neighbors = [0 for _ in range(len(region_points))] 

    for i in range(len(region_points)):
        for j in range(n_coplanar_points) : 
            # remember that coplanar points have x,y as ROWS and different points as cols
            distance = np.sqrt((region_points[i,0]-coplanar_points[j][0])**2 + (region_points[i,1]-coplanar_points[j][1])**2)
            if(distance<= search_r and distance>=2e-5): 
                # resolution_score[i] += distance_2
                region_neighbors[i] += 1
    return region_neighbors
    

def scan_region(region_start,region_end,coplanar_points,n_coplanar_points,number): 
    # This is just a wrapper function for find_neighbors
    region_neighbors = find_neighbors(region_start,region_end,coplanar_points,n_coplanar_points,number)
    np.savetxt('kinematics/analytical_equations/results/fullrange_n850/neighbors_region%d.csv'%number,region_neighbors, fmt ="%d", delimiter = ',')
    return 

    
def rotate_curve(x_points,y_points,theta): 
    # theta in degrees 
    theta = np.deg2rad(theta)
    for i in range(len(x_points)): 
        x_orig = x_points[i]
        y_orig = y_points[i]
        x_points[i] = x_orig*np.cos(theta) -y_orig*np.sin(theta)
        y_points[i] = y_orig*np.cos(theta) + x_orig*np.sin(theta) 
    
    return x_points,y_points

# coplanar_points = np.genfromtxt("kinematics/analytical_equations/results/fullrange_n500/coplanar_points.csv",delimiter = ',', dtype = np.float32)
filename = "kinematics/analytical_equations/results/fullrange_n850/coplanar_points.csv"
file = pd.read_csv(filename, engine = "pyarrow",header=None)
coplanar_points = np.array(file)
n_coplanar_points = len(coplanar_points)
print(n_coplanar_points)
# resolution_score = [0 for i in range(n_coplanar_points)] # total (SQUARED) distance of the points inside search_r of a setpoint. 
# neighbors = [0 for i in range(n_coplanar_points)] # number of points contained within a given distance of the point under investigation 


# Divide the number of points into equal, integer parts -- we can lose up to 9 points with this, but it is ok 
NPROCESSES = 8
region_distance = int(n_coplanar_points / NPROCESSES)
last_region_distance = n_coplanar_points%NPROCESSES


if __name__ == "__main__" : 
    
    start_time = time()
    processes = [] 
    for i in range(NPROCESSES): 
        p = multiprocessing.Process(target=scan_region, args=(i*region_distance,(i+1)*region_distance,coplanar_points,n_coplanar_points,i+1))
        p.start()
        processes.append(p)
        print("Started process %d"%i)

# last process separately  
    p = multiprocessing.Process(target=scan_region, args=(NPROCESSES*region_distance,NPROCESSES*region_distance+ last_region_distance,coplanar_points,n_coplanar_points,NPROCESSES+1))
    p.start()
    processes.append(p)
    for process in processes : 
        process.join()
    
    while(p.is_alive()):
        pass 

    end_time = time() 
    print("Elapsed time : %lf"%(end_time - start_time))