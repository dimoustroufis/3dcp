import numpy as np 
from scipy.optimize import fsolve, minimize 
import scipy 
import matplotlib.pyplot as plt 
from sympy import solve, Symbol
from sympy.abc import x,y,z 

class Delta: 
    def __init__(self,l,SB,sP,WB,wP,uP,UB,rail_height,bed_radius, printhead_radius) -> None:
        self.l = l 
        self.SB = SB 
        self.sP = sP
        self.WB = WB 
        self.wP = wP
        self.uP = uP
        self.UB = UB

        self.a = self.SB/2 - self.sP/2 
        self.b = self.WB - self.wP 
        self.c = self.uP - self.UB 

        self.rail_height = rail_height 
        self.bed_radius = bed_radius  
        self.printhead_radius = printhead_radius 

        # # Algebraic solution of the FKP equations (in order not to solve them every time fkp_pos is called)
        # self.L1  = Symbol('L1')
        # self.L2  =  Symbol('L2')
        # self.L3  =  Symbol('L3')
        # eq1 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 + 2*self.a*x + 2*self.b*y + 2*z*self.L1 + self.L1**2 -self.l**2
        # eq2 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 - 2*self.a*x + 2*self.b*y + 2*z*self.L2 + self.L2**2 -self.l**2
        # eq3 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 + 2*self.a*x - 2*self.b*y + 2*z*self.L3 + self.L3**2 -self.l**2  

        # equations= [eq1,eq2,eq3]
        # self.solutions = solve(equations,x,y,z,dict = True)

    
    def fkp_pos(self,l_1,l_2,l_3,xinit,yinit,zinit): 
        # Another way to get x,y,z is by solving the following non-linear system 
        def equations(vars): 
            x,y,z = vars 
            eq1 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 + 2*self.a*x + 2*self.b*y + 2*z*l_1 + l_1**2 -self.l**2
            eq2 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 - 2*self.a*x + 2*self.b*y + 2*z*l_2 + l_2**2 -self.l**2
            # eq3 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 + 2*self.a*x - 2*self.b*y + 2*z*l_3 + l_3**2 -self.l**2   
            eq3 = x**2 + y**2 + z**2 + self.c**2 + 2*self.c*y + 2*z*l_3 + l_3**2 - self.l**2 
            return [eq1,eq2,eq3]

        x_sol,y_sol,z_sol = fsolve(equations,(xinit,yinit,zinit))
        return [x_sol,y_sol,z_sol]  

    def fkp_pos_alt(self,l_1,l_2,l_3): 
        d = (l_2 - l_1)/2/self.a 
        e = (l_2**2 - l_1**2)/4/self.a
        D = (l_3 - l_1 - self.a*d)/(self.b - self.c)
        E = (self.c**2 - self.a**2 -self.b**2 -2*self.a*e + l_3**2 - l_1**2)/2/(self.b - self.c)

        A = d**2 + D**2 + 1 
        B = 2*(d*e + D*E + self.c*D +l_3)
        C = e**2 + E**2 + self.c**2 + 2*self.c*E + l_3**2 -self.l**2

        z = (-B + np.sqrt(B**2 - 4*A*C))/2/A
        # if(abs(z)>l_1 or abs(z)> l_2 or abs(z)>l_3): 
        if(z>0):
            z = (-B - np.sqrt(B**2 - 4*A*C))/2/A
        
        x = d*z + e 
        y = D*z + E


        return [x,y,z]



    def ikp_pos(self,x,y,z):
        c1 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 + 2*self.a*x + 2*self.b*y -self.l**2 
        c2 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 -2*self.a*x +2*self.b*y -self.l**2
        c3 = x**2 + y**2 + z**2 + self.c**2 + 2*self.c*y -self.l**2 
        # c3 = x**2 + y**2 + z**2 + self.a**2 + self.b**2 +2*self.a*x -2*self.b*y -self.l**2 
        L = [0,0,0] 
        if(z**2-c1<0 or z**2 -c2<0 or z**2-c3<0): 
            print("For [x,y,z] = [%lf,%lf,%lf] we have a negative root in IKP"%(x,y,z))
            return [-20,-20,-20]
        
        L[0] = -z - np.sqrt(z**2 -c1)
        # check for "knee-up" solution
        if(abs(L[0]) > abs(z)): 
            L[0] = -z +  np.sqrt(z**2 -c1)

        # check for "knee-up" solution
        L[1] = -z - np.sqrt(z**2 -c2)
        if(abs(L[1]) > abs(z)): 
            L[1] = -z + np.sqrt(z**2 -c2)

        # check for "knee-up" solution
        L[2] = -z - np.sqrt(z**2 -c3)
        if(abs(L[2]) > abs(z)): 
            L[2] = -z + np.sqrt(z**2 -c3)

        return L 

    def ikp_vel(self,x,y,z,xdot,ydot,zdot): 
        L = self.ikp_pos(x,y,z)
        Ldot = [0,0,0]
        Ldot[0] = -(xdot*(x+self.a)/(z+L[0]) + ydot*(y+self.b)/(z+L[1]) + zdot)
        Ldot[1] = -(xdot*(x-self.a)/(z+L[1]) + ydot*(y+self.b)/(z+L[2]) + zdot)
        Ldot[2] = -(xdot*(x/(z+L[2])) + ydot*(y+self.c)/(z+L[3]) + zdot)

        return L,Ldot



if __name__ == "__main__": 

    # SIMPLE TEST 

    l = 0.264 # arm length 
    SB = 0.246
    sP = 0.127
    WB = 0.071
    wP = 0.037
    uP = 0.073
    UB = 0.142


    
    delta = Delta(l,SB,sP,WB,wP,uP,UB)

    gridelements = 500
    # x = np.linspace(-SB-0.02,SB+0.02,gridelements)
    # y = np.linspace(-WB-0.05,WB+0.05,gridelements)
    x = np.linspace(-0.1,0.1,gridelements)
    y = np.linspace(-0.1,0.1,gridelements)
    error = np.zeros(shape=(gridelements,gridelements))

    for i in range(gridelements): 
        for j in range(gridelements): 
            nom_pos = [x[i],y[j],-0.4]
            nom_act_pos = delta.ikp_pos(nom_pos[0],nom_pos[1],nom_pos[2])
            real_act_pos = [0,0,0]
            for k in range(len(nom_act_pos)): 
                real_act_pos[k] = round(nom_act_pos[k],2)

            real_pos = delta.fkp_pos(real_act_pos[0],real_act_pos[1],real_act_pos[2],0,0,0)
            error[i][j] = np.sqrt((real_pos[0]-nom_pos[0])**2 + (real_pos[1]-nom_pos[1])**2 + (real_pos[2]-nom_pos[2])**2)
            # print(nom_pos)
            # print(real_pos)
            # print(error)
    
 
    plt.contourf(x,y,error)
    plt.title("Error vs x-position")
    plt.colorbar()
    plt.show()