import numpy as np 
import matplotlib.pyplot as plt
from tqdm import tqdm
from time import time, sleep
import pandas as pd 

import multiprocessing


# =====================================================================================
search_r_2 = 0.005**2 # 0.005 is the needed accuracy of the printer, and we square it to avoid root calculations 
def scan_region(region_start,region_end,coplanar_points,number):
    n_coplanar_points = len(coplanar_points[0])
    region_points = np.zeros(shape = (2,region_end - region_start))
    for i in range(len(region_points)):
        region_points[0,i] = coplanar_points[0][region_start+i]
        region_points[1,i] = coplanar_points[1][region_start+i]

    region_neighbors = [0 for i in range(len(region_points[0]))] 

    for i in tqdm(range(len(region_points[0]))):
        for j in range(n_coplanar_points) : 
            # remember that coplanar points have x,y as ROWS and different points as cols
            distance_2 = (region_points[0,i]-coplanar_points[0][j])**2 + (region_points[1,i]-coplanar_points[1][j])**2
            if(distance_2<= search_r_2 and distance_2!=0): # maybe re-phrase the !=0 to something like +-e-5
                # resolution_score[i] += distance_2
                region_neighbors[i] += 1
    np.savetxt('kinematics/analytical_equations/results/1_neighbors_region%d.csv'%number,region_neighbors, fmt ="%d", delimiter = ',')
    return 


def rotate_curve(x_points,y_points,theta): 
    # theta in degrees 
    theta = np.deg2rad(theta)
    for i in range(len(x_points)): 
        x_orig = x_points[i]
        y_orig = y_points[i]
        x_points[i] = x_orig*np.cos(theta) -y_orig*np.sin(theta)
        y_points[i] = y_orig*np.cos(theta) + x_orig*np.sin(theta) 
    
    return x_points,y_points

# coplanar_points = np.genfromtxt("kinematics/analytical_equations/results/coplanar_points.csv",delimiter = ',')
filename = "kinematics/analytical_equations/results/fullrange_n850/coplanar_points.csv"
file = pd.read_csv(filename, engine = "pyarrow",header=None)
coplanar_points = np.array(file)
if __name__ == "__main__" : 
    n_coplanar_points = len(coplanar_points)
    print(n_coplanar_points)
    resolution_score = [0 for i in range(n_coplanar_points)] # total (SQUARED) distance of the points inside search_r of a setpoint. 
    neighbors = [0 for i in range(n_coplanar_points)] # number of points contained within a given distance of the point under investigation 

    for i in tqdm(range(n_coplanar_points)): 
        for j in range(n_coplanar_points): 
            distance_2 = (coplanar_points[i,0]-coplanar_points[j,0])**2 + (coplanar_points[i,1]-coplanar_points[j,1])**2
            if(distance_2<= search_r_2 and distance_2!=0): # maybe re-phrase the !=0 to something like +-e-5
                resolution_score[i] += distance_2
                neighbors[i] += 1 

    np.savetxt('kinematics/analytical_equations/results/full_problem_neighbors_single.csv',neighbors, fmt ="%d", delimiter = ',')
