import numpy as np 
from tqdm import tqdm 
from time import time 
import matplotlib.pyplot as plt 

l = 1.49650 # arm length 
SB = 1.626695
sP = 0.147224

l = 0.264 # from a real design 
SB = 0.246 # from a real design 
sP = 0.127 # from a real design 

WB = np.sqrt(3)/6 * SB 
UB = np.sqrt(3)/3 * SB 
wP = np.sqrt(3)/6 * sP 
uP = np.sqrt(3)/3 * sP 

rail_height = 2.710 
bed_radius = 1.430/2 
printhead_radius = 0.085 

bed_radius = WB 

NPROCESSES  =  6 

# TODO : Speed up the loading part. Takes up to 2 minutes for a 300-node analysis
print("Starting loading")
load_time_start = time()

region1 = np.genfromtxt("kinematics/analytical_equations/results/1_region1.csv", delimiter=",")
print("Done region 1")
region2 = np.genfromtxt("kinematics/analytical_equations/results/1_region2.csv", delimiter=",")
print("Done region 2")
region3 = np.genfromtxt("kinematics/analytical_equations/results/1_region3.csv", delimiter=",")
print("Done region 3")
region4 = np.genfromtxt("kinematics/analytical_equations/results/1_region4.csv", delimiter=",")
print("Done region 4")
region5 = np.genfromtxt("kinematics/analytical_equations/results/1_region5.csv", delimiter=",")
print("Done region 5")
region6 = np.genfromtxt("kinematics/analytical_equations/results/1_region6.csv", delimiter=",")
print("Done region 6")
# region7 = np.genfromtxt("kinematics/analytical_equations/results/1_region7.csv", delimiter=",")
# region8 = np.genfromtxt("kinematics/analytical_equations/results/1_region8.csv", delimiter=",")
# region9 = np.genfromtxt("kinematics/analytical_equations/results/1_region9.csv", delimiter=",")
# region10 = np.genfromtxt("kinematics/analytical_equations/results/1_region10.csv", delimiter=",")
# region11 = np.genfromtxt("kinematics/analytical_equations/results/1_region11.csv", delimiter=",")
# region12 = np.genfromtxt("kinematics/analytical_equations/results/1_region12.csv", delimiter=",")
load_time_end = time()
print("Loading time = %lf"%(load_time_end-load_time_start))

workspace = np.append(region1,region2,axis =1)
workspace = np.append(workspace,region3, axis =1)
workspace = np.append(workspace,region4, axis =1)
workspace = np.append(workspace,region5, axis =1)
workspace = np.append(workspace,region6, axis =1)
# workspace = np.append(workspace,region7, axis =1)
# workspace = np.append(workspace,region8, axis =1)
# workspace = np.append(workspace,region9, axis =1)
# workspace = np.append(workspace,region10, axis =1)
# workspace = np.append(workspace,region11, axis =1)
# workspace = np.append(workspace,region12, axis =1)


z_height = -2.0
# z_height = -0.5 # from a real design
# z_tolerance = 0.005
z_tolerance = 0 
# z_tolerance = 0.01 # from a real design 
coplanar_points = [[],[]]
for i in tqdm(range(len(workspace[0]))):
    # if(workspace[2,i]>z_height-z_tolerance and workspace[2,i]<z_height+z_tolerance): 
    if(workspace[2,i]>z_height and workspace[2,i]<z_height+z_tolerance): 
    # if (points[2][i] == z_height): 
        coplanar_points[0].append(workspace[0,i])
        coplanar_points[1].append(workspace[1,i])

np.savetxt("kinematics/analytical_equations/results/1_coplanar_points.csv", coplanar_points, fmt = "%.4lf", delimiter=",")


fig,ax = plt.subplots()
ax.set_title("Points on plane Z = %lf"%(z_height))
ax.scatter(coplanar_points[0],coplanar_points[1],s=0.5)

x_towers = [-SB/2,SB/2,0,-SB/2] # fourth point just to close the triangle on the plot
y_towers = [-WB,-WB,UB,-WB] # fourth point just to close the triangle on the plot
# x_towers , y_towers = rotate_curve(x_towers,y_towers,30)

plt.plot(x_towers,y_towers,c="k",linestyle = "--")
# ax.set_xlim([-0.13,+0.13])
# ax.set_ylim([-0.075,+0.1])

bed_plot = plt.Circle( (0,0),bed_radius,fill = False )
ax.set_aspect( 1 )
ax.add_artist(bed_plot)

ax.scatter([0],[0],c="k",s=20)
# plt.rcParams['figure.dpi'] = 300
plt.show()
