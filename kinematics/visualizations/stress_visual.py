import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
import os 
from delta import Delta 
from matplotlib import animation


NCP = 6 
NNODES = 100 
def bezier_6(x_cp, y_cp): 
    
    # Calculate x and y coefficients of the x(t) and y(t) polynomials 
    x_coeffs = [] 
    y_coeffs = []
    t_points = np.linspace(0,1,NNODES)
    bezier_matrix =  [[1,-5,10,-10,5,-1],[0,5,-20,30,-20,5],[0,0,10,-30,30,-10],[0,0,0,10,-20,10],[0,0,0,0,5,-5],[0,0,0,0,0,1]]
    for i in range(NCP) : 
        sigma_x = 0 
        sigma_y = 0 

        for j in range(NCP): 
            sigma_x = sigma_x  + x_cp[j]*bezier_matrix[j][i]
            sigma_y = sigma_y  + y_cp[j]*bezier_matrix[j][i]
        
        x_coeffs.append(sigma_x)
        y_coeffs.append(sigma_y)

    # Genrate x and y points from the x(t) and y(t) polynomials
    x_points = np.zeros(NNODES)
    y_points = np.zeros(NNODES)
    
    for i in range(NNODES):
        for j in range(NCP): 
            y_points[i] = y_points[i] + y_coeffs[j]*t_points[i]**j
            x_points[i] = x_points[i] + x_coeffs[j]*t_points[i]**j
    
    return x_points,y_points

def rotate_curve(x_points,y_points,theta): 
    # theta in degrees 
    theta = np.deg2rad(theta)
    for i in range(len(x_points)): 
        x_orig = x_points[i]
        y_orig = y_points[i]
        x_points[i] = x_orig*np.cos(theta) -y_orig*np.sin(theta)
        y_points[i] = y_orig*np.cos(theta) + x_orig*np.sin(theta) 
    
    return x_points,y_points



# Scale MTLAB-generated Control Points
x_cp = [1,1.2,1.5689,1.8303,2.2987,2.9993]
y_cp = [2,3.353,2.9048,-0.1,2.9582,2.4778]
x_cp = [i/max(x_cp) - 0.6 for i in x_cp]
y_cp = [i/max(y_cp) - 0.5 for i in y_cp]

x_points_top,y_points_top = bezier_6(x_cp,y_cp)

# Add right part closure section 
x_right_cp = [x_cp[-1],0.3,0.15,0,0.3,x_cp[-1]]
y_right_cp = [y_cp[-1],0.18, 0, 0, 0,-y_cp[-1]]
x_right_points , y_right_points = bezier_6(x_right_cp,y_right_cp)
x_points = np.append(x_points_top,x_right_points)
y_points = np.append(y_points_top,y_right_points)


# Add axisymmetric points 
x_points = np.append(x_points,np.flip(x_points_top))
y_points = np.append(y_points,np.flip(-y_points_top))


# Add left part closure section 
x_left_cp = [x_cp[0],-0.4,-0.6,-0.5,-0.4,x_cp[0]]
y_left_cp = [y_cp[0],0.1,0,0,-0.1,-y_cp[0]]
x_left_points , y_left_points = bezier_6(x_left_cp,y_left_cp)
x_points = np.append(x_points,np.flip(x_left_points))
y_points = np.append(y_points,np.flip(y_left_points))

# Rotate curve - angle in degrees 
x_points,y_points = rotate_curve(x_points,y_points,-90)
x_points = [i*1.7 for i in x_points]




NPOINTS = len(x_points) # Greater than NNODES, since other points were added
u_print = 0.2 # print velocity in [TBD] from the project requirement sheet
time = np.zeros(NPOINTS)
t = []

# First point for time = 0 
time = 0 
t.append(time)

# All other points
for i in range(1,NPOINTS): 
    # time at each point is the total previous time plus the time from the previous point to the current point
    p2p_dist = np.sqrt((x_points[i] - x_points[i-1])**2 + (y_points[i]-y_points[i-1])**2)
    time = time + p2p_dist/u_print
    t.append(time)

t = np.array(t)

def animate_func(num):
    ax.clear()
    ax.plot(x_points[:num+1], y_points[:num+1]) # update point location
    ax.set_xlim([-0.5,0.5])
    ax.set_ylim([-0.5,0.5])
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.grid(linestyle = "--")
    ax.set_title('Trajectory \nTime = ' + str(np.round(t[num],decimals=2)) + ' sec')

fig = plt.figure()
ax = plt.axes()
line_ani = animation.FuncAnimation(fig,animate_func, interval = 1e-6, frames = NPOINTS)
# line_ani = animation.FuncAnimation(fig,animate_func)
plt.show()
f = "kinematics/visualizations/trajectory.gif"
writergif = animation.PillowWriter(fps=NPOINTS/21)
line_ani.save(f, writer=writergif)


