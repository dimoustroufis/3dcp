% Simscape(TM) Multibody(TM) version: 7.5

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(163).translation = [0.0 0.0 0.0];
smiData.RigidTransform(163).angle = 0.0;
smiData.RigidTransform(163).axis = [0.0 0.0 0.0];
smiData.RigidTransform(163).ID = "";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(1).angle = 0;  % rad
smiData.RigidTransform(1).axis = [0 0 0];
smiData.RigidTransform(1).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Auger_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(2).angle = 0;  % rad
smiData.RigidTransform(2).axis = [0 0 0];
smiData.RigidTransform(2).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Hopper_v2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(3).angle = 0;  % rad
smiData.RigidTransform(3).axis = [0 0 0];
smiData.RigidTransform(3).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Nozzle_cap_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(4).angle = 0;  % rad
smiData.RigidTransform(4).axis = [0 0 0];
smiData.RigidTransform(4).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.bearing_and_motor_cap_v1_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(5).angle = 0;  % rad
smiData.RigidTransform(5).axis = [0 0 0];
smiData.RigidTransform(5).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Ball_joint_base_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(6).angle = 0;  % rad
smiData.RigidTransform(6).axis = [0 0 0];
smiData.RigidTransform(6).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Ball_joint_base_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(7).angle = 0;  % rad
smiData.RigidTransform(7).axis = [0 0 0];
smiData.RigidTransform(7).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Nozzle_clamp_v1_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(8).angle = 0;  % rad
smiData.RigidTransform(8).axis = [0 0 0];
smiData.RigidTransform(8).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Nozzle_clamp_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(9).angle = 0;  % rad
smiData.RigidTransform(9).axis = [0 0 0];
smiData.RigidTransform(9).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.bearing_and_motor_cap_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(10).angle = 0;  % rad
smiData.RigidTransform(10).axis = [0 0 0];
smiData.RigidTransform(10).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Ball_joint_base_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [-0.433666259 -0.42750559999999999 1.5664000220000001];  % m
smiData.RigidTransform(11).angle = 0;  % rad
smiData.RigidTransform(11).axis = [0 0 0];
smiData.RigidTransform(11).ID = "G[root-M1DUpUFaKilJRLDZQ.MCCHCli1nBgt5BfPj:-:Extruder_Assembly_1.Extruder_plate_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(12).angle = 0;  % rad
smiData.RigidTransform(12).axis = [0 0 0];
smiData.RigidTransform(12).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(13).angle = 0;  % rad
smiData.RigidTransform(13).axis = [0 0 0];
smiData.RigidTransform(13).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(14).angle = 0;  % rad
smiData.RigidTransform(14).axis = [0 0 0];
smiData.RigidTransform(14).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(15).angle = 0;  % rad
smiData.RigidTransform(15).axis = [0 0 0];
smiData.RigidTransform(15).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Bearing_plate_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(16).angle = 0;  % rad
smiData.RigidTransform(16).axis = [0 0 0];
smiData.RigidTransform(16).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Ball_joint_base_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(17).angle = 0;  % rad
smiData.RigidTransform(17).axis = [0 0 0];
smiData.RigidTransform(17).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(18).angle = 0;  % rad
smiData.RigidTransform(18).axis = [0 0 0];
smiData.RigidTransform(18).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_4_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(19).angle = 0;  % rad
smiData.RigidTransform(19).axis = [0 0 0];
smiData.RigidTransform(19).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_2_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(20).angle = 0;  % rad
smiData.RigidTransform(20).axis = [0 0 0];
smiData.RigidTransform(20).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_1_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [0.036391900999999997 0.10106538700000001 -0.24245523099999999];  % m
smiData.RigidTransform(21).angle = 0;  % rad
smiData.RigidTransform(21).axis = [0 0 0];
smiData.RigidTransform(21).ID = "G[root-M5HyekA2D7srTVbBZ.MT+I2pGP35qDSTYvs:-:Slider_Assembly_1.Cariage_6709K12_v1_4_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(22).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(22).axis = [0 -0 1];
smiData.RigidTransform(22).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(23).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(23).axis = [0 -0 1];
smiData.RigidTransform(23).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Ball_joint_base_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(24).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(24).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(24).axis = [0 -0 1];
smiData.RigidTransform(24).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(25).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(25).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(25).axis = [0 -0 1];
smiData.RigidTransform(25).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_4_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(26).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(26).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(26).axis = [0 -0 1];
smiData.RigidTransform(26).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(27).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(27).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(27).axis = [0 -0 1];
smiData.RigidTransform(27).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Bearing_plate_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(28).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(28).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(28).axis = [0 -0 1];
smiData.RigidTransform(28).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_4_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(29).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(29).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(29).axis = [0 -0 1];
smiData.RigidTransform(29).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_1_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(30).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(30).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(30).axis = [0 -0 1];
smiData.RigidTransform(30).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(31).translation = [0.378522101 1.3774986380000001 -0.21376767699999999];  % m
smiData.RigidTransform(31).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(31).axis = [0 -0 1];
smiData.RigidTransform(31).ID = "G[root-M5QjUULteb+srYC77.MT+I2pGP35qDSTYvs:-:Slider_Assembly_2.Cariage_6709K12_v1_2_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(32).translation = [0.001065777 0.175765118 -0.10462299999999999];  % m
smiData.RigidTransform(32).angle = 2.6911987879015622;  % rad
smiData.RigidTransform(32).axis = [0.024200088487030435 -0.50863193031851295 0.86064389568373934];
smiData.RigidTransform(32).ID = "G[root-M6mYKQ45uMzHf/HeO.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_4.Shell_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(33).translation = [0.001065777 0.175765118 -0.10462299999999999];  % m
smiData.RigidTransform(33).angle = 2.6911987879015622;  % rad
smiData.RigidTransform(33).axis = [0.024200088487030435 -0.50863193031851295 0.86064389568373934];
smiData.RigidTransform(33).ID = "G[root-M6mYKQ45uMzHf/HeO.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_4.M10_Rod_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(34).translation = [0.001065777 0.175765118 -0.10462299999999999];  % m
smiData.RigidTransform(34).angle = 2.6911987879015622;  % rad
smiData.RigidTransform(34).axis = [0.024200088487030435 -0.50863193031851295 0.86064389568373934];
smiData.RigidTransform(34).ID = "G[root-M6mYKQ45uMzHf/HeO.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_4.Shell_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(35).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(35).angle = 0;  % rad
smiData.RigidTransform(35).axis = [0 0 0];
smiData.RigidTransform(35).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Top_axle_support_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(36).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(36).angle = 0;  % rad
smiData.RigidTransform(36).axis = [0 0 0];
smiData.RigidTransform(36).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(37).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(37).angle = 0;  % rad
smiData.RigidTransform(37).axis = [0 0 0];
smiData.RigidTransform(37).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_assembly_connection_bottom_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(38).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(38).angle = 0;  % rad
smiData.RigidTransform(38).axis = [0 0 0];
smiData.RigidTransform(38).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(39).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(39).angle = 0;  % rad
smiData.RigidTransform(39).axis = [0 0 0];
smiData.RigidTransform(39).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_5_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(40).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(40).angle = 0;  % rad
smiData.RigidTransform(40).axis = [0 0 0];
smiData.RigidTransform(40).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axis_main_base_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(41).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(41).angle = 0;  % rad
smiData.RigidTransform(41).axis = [0 0 0];
smiData.RigidTransform(41).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_3_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(42).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(42).angle = 0;  % rad
smiData.RigidTransform(42).axis = [0 0 0];
smiData.RigidTransform(42).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_back_support_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(43).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(43).angle = 0;  % rad
smiData.RigidTransform(43).axis = [0 0 0];
smiData.RigidTransform(43).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Rail_6709K301_v1_6_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(44).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(44).angle = 0;  % rad
smiData.RigidTransform(44).axis = [0 0 0];
smiData.RigidTransform(44).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Rail_6709K301_v1_7_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(45).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(45).angle = 0;  % rad
smiData.RigidTransform(45).axis = [0 0 0];
smiData.RigidTransform(45).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_10_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(46).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(46).angle = 0;  % rad
smiData.RigidTransform(46).axis = [0 0 0];
smiData.RigidTransform(46).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_19_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(47).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(47).angle = 0;  % rad
smiData.RigidTransform(47).axis = [0 0 0];
smiData.RigidTransform(47).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_23_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(48).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(48).angle = 0;  % rad
smiData.RigidTransform(48).axis = [0 0 0];
smiData.RigidTransform(48).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(49).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(49).angle = 0;  % rad
smiData.RigidTransform(49).axis = [0 0 0];
smiData.RigidTransform(49).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_11_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(50).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(50).angle = 0;  % rad
smiData.RigidTransform(50).axis = [0 0 0];
smiData.RigidTransform(50).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_8_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(51).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(51).angle = 0;  % rad
smiData.RigidTransform(51).axis = [0 0 0];
smiData.RigidTransform(51).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(52).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(52).angle = 0;  % rad
smiData.RigidTransform(52).axis = [0 0 0];
smiData.RigidTransform(52).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_assembly_connection_top_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(53).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(53).angle = 0;  % rad
smiData.RigidTransform(53).axis = [0 0 0];
smiData.RigidTransform(53).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_6_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(54).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(54).angle = 0;  % rad
smiData.RigidTransform(54).axis = [0 0 0];
smiData.RigidTransform(54).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_15_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(55).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(55).angle = 0;  % rad
smiData.RigidTransform(55).axis = [0 0 0];
smiData.RigidTransform(55).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_4_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(56).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(56).angle = 0;  % rad
smiData.RigidTransform(56).axis = [0 0 0];
smiData.RigidTransform(56).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_base_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(57).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(57).angle = 0;  % rad
smiData.RigidTransform(57).axis = [0 0 0];
smiData.RigidTransform(57).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_21_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(58).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(58).angle = 0;  % rad
smiData.RigidTransform(58).axis = [0 0 0];
smiData.RigidTransform(58).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_7_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(59).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(59).angle = 0;  % rad
smiData.RigidTransform(59).axis = [0 0 0];
smiData.RigidTransform(59).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_back_support_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(60).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(60).angle = 0;  % rad
smiData.RigidTransform(60).axis = [0 0 0];
smiData.RigidTransform(60).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_assembly_connection_bottom_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(61).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(61).angle = 0;  % rad
smiData.RigidTransform(61).axis = [0 0 0];
smiData.RigidTransform(61).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_3_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(62).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(62).angle = 0;  % rad
smiData.RigidTransform(62).axis = [0 0 0];
smiData.RigidTransform(62).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_8_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(63).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(63).angle = 0;  % rad
smiData.RigidTransform(63).axis = [0 0 0];
smiData.RigidTransform(63).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_5_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(64).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(64).angle = 0;  % rad
smiData.RigidTransform(64).axis = [0 0 0];
smiData.RigidTransform(64).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_22_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(65).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(65).angle = 0;  % rad
smiData.RigidTransform(65).axis = [0 0 0];
smiData.RigidTransform(65).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_base_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(66).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(66).angle = 0;  % rad
smiData.RigidTransform(66).axis = [0 0 0];
smiData.RigidTransform(66).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_13_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(67).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(67).angle = 0;  % rad
smiData.RigidTransform(67).axis = [0 0 0];
smiData.RigidTransform(67).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_assembly_connection_top_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(68).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(68).angle = 0;  % rad
smiData.RigidTransform(68).axis = [0 0 0];
smiData.RigidTransform(68).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(69).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(69).angle = 0;  % rad
smiData.RigidTransform(69).axis = [0 0 0];
smiData.RigidTransform(69).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_12_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(70).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(70).angle = 0;  % rad
smiData.RigidTransform(70).axis = [0 0 0];
smiData.RigidTransform(70).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Part_61_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(71).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(71).angle = 0;  % rad
smiData.RigidTransform(71).axis = [0 0 0];
smiData.RigidTransform(71).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_assembly_connection_bottom_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(72).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(72).angle = 0;  % rad
smiData.RigidTransform(72).axis = [0 0 0];
smiData.RigidTransform(72).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Top_axle_support_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(73).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(73).angle = 0;  % rad
smiData.RigidTransform(73).axis = [0 0 0];
smiData.RigidTransform(73).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Top_axle_support_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(74).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(74).angle = 0;  % rad
smiData.RigidTransform(74).axis = [0 0 0];
smiData.RigidTransform(74).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Circle_print_area_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(75).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(75).angle = 0;  % rad
smiData.RigidTransform(75).axis = [0 0 0];
smiData.RigidTransform(75).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Part_60_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(76).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(76).angle = 0;  % rad
smiData.RigidTransform(76).axis = [0 0 0];
smiData.RigidTransform(76).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_16_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(77).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(77).angle = 0;  % rad
smiData.RigidTransform(77).axis = [0 0 0];
smiData.RigidTransform(77).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_6_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(78).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(78).angle = 0;  % rad
smiData.RigidTransform(78).axis = [0 0 0];
smiData.RigidTransform(78).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Rail_6709K301_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(79).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(79).angle = 0;  % rad
smiData.RigidTransform(79).axis = [0 0 0];
smiData.RigidTransform(79).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axis_main_base_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(80).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(80).angle = 0;  % rad
smiData.RigidTransform(80).axis = [0 0 0];
smiData.RigidTransform(80).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Part_59_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(81).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(81).angle = 0;  % rad
smiData.RigidTransform(81).axis = [0 0 0];
smiData.RigidTransform(81).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Rail_6709K301_v1_3_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(82).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(82).angle = 0;  % rad
smiData.RigidTransform(82).axis = [0 0 0];
smiData.RigidTransform(82).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axis_main_base_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(83).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(83).angle = 0;  % rad
smiData.RigidTransform(83).axis = [0 0 0];
smiData.RigidTransform(83).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Rail_6709K301_v1_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(84).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(84).angle = 0;  % rad
smiData.RigidTransform(84).axis = [0 0 0];
smiData.RigidTransform(84).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Rail_6709K301_v1_4_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(85).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(85).angle = 0;  % rad
smiData.RigidTransform(85).axis = [0 0 0];
smiData.RigidTransform(85).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(86).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(86).angle = 0;  % rad
smiData.RigidTransform(86).axis = [0 0 0];
smiData.RigidTransform(86).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_base_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(87).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(87).angle = 0;  % rad
smiData.RigidTransform(87).axis = [0 0 0];
smiData.RigidTransform(87).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_big_7_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(88).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(88).angle = 0;  % rad
smiData.RigidTransform(88).axis = [0 0 0];
smiData.RigidTransform(88).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_back_support_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(89).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(89).angle = 0;  % rad
smiData.RigidTransform(89).axis = [0 0 0];
smiData.RigidTransform(89).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_4_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(90).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(90).angle = 0;  % rad
smiData.RigidTransform(90).axis = [0 0 0];
smiData.RigidTransform(90).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_9_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(91).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(91).angle = 0;  % rad
smiData.RigidTransform(91).axis = [0 0 0];
smiData.RigidTransform(91).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_20_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(92).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(92).angle = 0;  % rad
smiData.RigidTransform(92).axis = [0 0 0];
smiData.RigidTransform(92).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_14_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(93).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(93).angle = 0;  % rad
smiData.RigidTransform(93).axis = [0 0 0];
smiData.RigidTransform(93).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_17_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(94).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(94).angle = 0;  % rad
smiData.RigidTransform(94).axis = [0 0 0];
smiData.RigidTransform(94).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Axle_assembly_connection_top_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(95).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(95).angle = 0;  % rad
smiData.RigidTransform(95).axis = [0 0 0];
smiData.RigidTransform(95).ID = "G[root-M8WyaXjmrCxgimslg.MlteZHnwyUxVJFWXC:-:Fixed_Assembly_1.Reinforcement_small_18_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(96).translation = [0.011658696 -1.405223847 -0.58185366199999999];  % m
smiData.RigidTransform(96).angle = 0.29761659185494554;  % rad
smiData.RigidTransform(96).axis = [0.076559297129442302 -0.37807093456631363 0.9226055725277188];
smiData.RigidTransform(96).ID = "G[root-MB0/jg/EbduG+dogU.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_3.Shell_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(97).translation = [0.011658696 -1.405223847 -0.58185366199999999];  % m
smiData.RigidTransform(97).angle = 0.29761659185494554;  % rad
smiData.RigidTransform(97).axis = [0.076559297129442302 -0.37807093456631363 0.9226055725277188];
smiData.RigidTransform(97).ID = "G[root-MB0/jg/EbduG+dogU.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_3.M10_Rod_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(98).translation = [0.011658696 -1.405223847 -0.58185366199999999];  % m
smiData.RigidTransform(98).angle = 0.29761659185494554;  % rad
smiData.RigidTransform(98).axis = [0.076559297129442302 -0.37807093456631363 0.9226055725277188];
smiData.RigidTransform(98).ID = "G[root-MB0/jg/EbduG+dogU.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_3.Shell_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(99).translation = [0.66280485499999997 -0.91272399500000001 -0.18411360299999999];  % m
smiData.RigidTransform(99).angle = 2.0023846997601651;  % rad
smiData.RigidTransform(99).axis = [-0.46440738440169776 -0.27201624791664963 0.84281251899964171];
smiData.RigidTransform(99).ID = "G[root-MIFB1upuECeSPQ8Qj.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_5.M10_Rod_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(100).translation = [0.66280485499999997 -0.91272399500000001 -0.18411360299999999];  % m
smiData.RigidTransform(100).angle = 2.0023846997601651;  % rad
smiData.RigidTransform(100).axis = [-0.46440738440169776 -0.27201624791664963 0.84281251899964171];
smiData.RigidTransform(100).ID = "G[root-MIFB1upuECeSPQ8Qj.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_5.Shell_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(101).translation = [0.66280485499999997 -0.91272399500000001 -0.18411360299999999];  % m
smiData.RigidTransform(101).angle = 2.0023846997601651;  % rad
smiData.RigidTransform(101).axis = [-0.46440738440169776 -0.27201624791664963 0.84281251899964171];
smiData.RigidTransform(101).ID = "G[root-MIFB1upuECeSPQ8Qj.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_5.Shell_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(102).translation = [0.33808329599999998 -2.710549442 -1.1991148549999999];  % m
smiData.RigidTransform(102).angle = 1.571659438273761;  % rad
smiData.RigidTransform(102).axis = [-0.1606953423765469 0.13248647386566578 0.97807174643843309];
smiData.RigidTransform(102).ID = "G[root-MIb+flQx/XevQtr4C.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_1.Shell_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(103).translation = [0.33808329599999998 -2.710549442 -1.1991148549999999];  % m
smiData.RigidTransform(103).angle = 1.571659438273761;  % rad
smiData.RigidTransform(103).axis = [-0.1606953423765469 0.13248647386566578 0.97807174643843309];
smiData.RigidTransform(103).ID = "G[root-MIb+flQx/XevQtr4C.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_1.Shell_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(104).translation = [0.33808329599999998 -2.710549442 -1.1991148549999999];  % m
smiData.RigidTransform(104).angle = 1.571659438273761;  % rad
smiData.RigidTransform(104).axis = [-0.1606953423765469 0.13248647386566578 0.97807174643843309];
smiData.RigidTransform(104).ID = "G[root-MIb+flQx/XevQtr4C.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_1.M10_Rod_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(105).translation = [0.54667452699999997 -0.63058277299999999 -0.24901693499999999];  % m
smiData.RigidTransform(105).angle = 1.9837236424833518;  % rad
smiData.RigidTransform(105).axis = [-0.46927885311196449 -0.27447758161661712 0.83930889141710663];
smiData.RigidTransform(105).ID = "G[root-MYiVmJ1k9sih0Rs/B.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_6.Shell_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(106).translation = [0.54667452699999997 -0.63058277299999999 -0.24901693499999999];  % m
smiData.RigidTransform(106).angle = 1.9837236424833518;  % rad
smiData.RigidTransform(106).axis = [-0.46927885311196449 -0.27447758161661712 0.83930889141710663];
smiData.RigidTransform(106).ID = "G[root-MYiVmJ1k9sih0Rs/B.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_6.M10_Rod_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(107).translation = [0.54667452699999997 -0.63058277299999999 -0.24901693499999999];  % m
smiData.RigidTransform(107).angle = 1.9837236424833518;  % rad
smiData.RigidTransform(107).axis = [-0.46927885311196449 -0.27447758161661712 0.83930889141710663];
smiData.RigidTransform(107).ID = "G[root-MYiVmJ1k9sih0Rs/B.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_6.Shell_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(108).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(108).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(108).axis = [-0 0 -1];
smiData.RigidTransform(108).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_4_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(109).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(109).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(109).axis = [-0 0 -1];
smiData.RigidTransform(109).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_2_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(110).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(110).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(110).axis = [-0 0 -1];
smiData.RigidTransform(110).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_4_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(111).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(111).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(111).axis = [-0 0 -1];
smiData.RigidTransform(111).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Bearing_plate_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(112).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(112).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(112).axis = [-0 0 -1];
smiData.RigidTransform(112).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(113).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(113).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(113).axis = [-0 0 -1];
smiData.RigidTransform(113).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Ball_joint_base_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(114).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(114).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(114).axis = [-0 0 -1];
smiData.RigidTransform(114).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(115).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(115).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(115).axis = [-0 0 -1];
smiData.RigidTransform(115).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(116).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(116).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(116).axis = [-0 0 -1];
smiData.RigidTransform(116).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_1_2_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(117).translation = [0.36726935599999999 1.0659149489999999 0.039458888999999997];  % m
smiData.RigidTransform(117).angle = 2.0943951021776339;  % rad
smiData.RigidTransform(117).axis = [-0 0 -1];
smiData.RigidTransform(117).ID = "G[root-Mle52KEc5P6JKZBsm.MT+I2pGP35qDSTYvs:-:Slider_Assembly_3.Cariage_6709K12_v1_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(118).translation = [0.036934466999999999 -3.246841538 -0.63787065399999998];  % m
smiData.RigidTransform(118).angle = 2.0114011920156769;  % rad
smiData.RigidTransform(118).axis = [0.0058083844051754693 0.025301219090691295 0.99966299870662745];
smiData.RigidTransform(118).ID = "G[root-MwP2t+ZQ7SN8Npaqe.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_2.M10_Rod_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(119).translation = [0.036934466999999999 -3.246841538 -0.63787065399999998];  % m
smiData.RigidTransform(119).angle = 2.0114011920156769;  % rad
smiData.RigidTransform(119).axis = [0.0058083844051754693 0.025301219090691295 0.99966299870662745];
smiData.RigidTransform(119).ID = "G[root-MwP2t+ZQ7SN8Npaqe.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_2.Shell_2_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(120).translation = [0.036934466999999999 -3.246841538 -0.63787065399999998];  % m
smiData.RigidTransform(120).angle = 2.0114011920156769;  % rad
smiData.RigidTransform(120).axis = [0.0058083844051754693 0.025301219090691295 0.99966299870662745];
smiData.RigidTransform(120).ID = "G[root-MwP2t+ZQ7SN8Npaqe.MIV7ddSL8JeNBNeR8:-:Rod_Assembly_2.Shell_1_1_RIGID]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(121).translation = [0.040000000000000001 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(121).angle = 2.4141525735714602;  % rad
smiData.RigidTransform(121).axis = [0.66478053422056338 0.37120923291011854 0.64828276756651781];
smiData.RigidTransform(121).ID = "B[929a236b5007c36d64decd36:MEmCWrSl4Ub5Gt1oM:-:929a236b5007c36d64decd36:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(122).translation = [0.040000000000000001 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(122).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(122).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(122).ID = "F[929a236b5007c36d64decd36:MEmCWrSl4Ub5Gt1oM:-:929a236b5007c36d64decd36:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(123).translation = [0.040000731999999997 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(123).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(123).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(123).ID = "B[929a236b5007c36d64decd36:Mh0DTzD1xS0HzvUFp:-:929a236b5007c36d64decd36:MVMOwACUcn0nCEBsD]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(124).translation = [0.040000731999999997 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(124).angle = 2.4141525735714602;  % rad
smiData.RigidTransform(124).axis = [0.66478053422056338 0.37120923291011854 0.64828276756651781];
smiData.RigidTransform(124).ID = "F[929a236b5007c36d64decd36:Mh0DTzD1xS0HzvUFp:-:929a236b5007c36d64decd36:MVMOwACUcn0nCEBsD]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(125).translation = [0.46248725899999998 0.61619666399999995 -0.63957018700000001];  % m
smiData.RigidTransform(125).angle = 2.7734925708972393;  % rad
smiData.RigidTransform(125).axis = [-0.18615678787662607 0.69474659060964694 -0.69474659060964705];
smiData.RigidTransform(125).ID = "B[M1DUpUFaKilJRLDZQ:MQf+4UUiE8K3vjbfU:-:MIb+flQx/XevQtr4C:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(126).translation = [0.028750000000000001 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(126).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(126).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(126).ID = "F[M1DUpUFaKilJRLDZQ:MQf+4UUiE8K3vjbfU:-:MIb+flQx/XevQtr4C:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(127).translation = [0.49123725899999998 0.56640020400000002 -0.63957018700000001];  % m
smiData.RigidTransform(127).angle = 2.7734925708972393;  % rad
smiData.RigidTransform(127).axis = [0.18615678787662607 -0.69474659060964694 -0.69474659060964705];
smiData.RigidTransform(127).ID = "B[M1DUpUFaKilJRLDZQ:MQf+4UUiE8K3vjbfU:-:MwP2t+ZQ7SN8Npaqe:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(128).translation = [0.051249999999999997 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(128).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(128).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(128).ID = "F[M1DUpUFaKilJRLDZQ:MQf+4UUiE8K3vjbfU:-:MwP2t+ZQ7SN8Npaqe:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(129).translation = [0.3745001 0.46379843399999998 -0.63957018700000001];  % m
smiData.RigidTransform(129).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(129).axis = [-0.57735026918962584 0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(129).ID = "B[M1DUpUFaKilJRLDZQ:MfdR481XY4rW/xg+L:-:M6mYKQ45uMzHf/HeO:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(130).translation = [0.028750000000000001 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(130).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(130).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(130).ID = "F[M1DUpUFaKilJRLDZQ:MfdR481XY4rW/xg+L:-:M6mYKQ45uMzHf/HeO:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(131).translation = [0.4320001 0.46379843399999998 -0.63957018700000001];  % m
smiData.RigidTransform(131).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(131).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(131).ID = "B[M1DUpUFaKilJRLDZQ:MfdR481XY4rW/xg+L:-:MB0/jg/EbduG+dogU:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(132).translation = [0.028750000000000001 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(132).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(132).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(132).ID = "F[M1DUpUFaKilJRLDZQ:MfdR481XY4rW/xg+L:-:MB0/jg/EbduG+dogU:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(133).translation = [0.34401294100000002 0.61619666399999995 -0.63957018700000001];  % m
smiData.RigidTransform(133).angle = 2.7734925708972393;  % rad
smiData.RigidTransform(133).axis = [-0.18615678787662607 -0.69474659060964694 0.69474659060964705];
smiData.RigidTransform(133).ID = "B[M1DUpUFaKilJRLDZQ:Mg+9CbpCP8+ID766b:-:MIFB1upuECeSPQ8Qj:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(134).translation = [0.028750000000000001 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(134).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(134).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(134).ID = "F[M1DUpUFaKilJRLDZQ:Mg+9CbpCP8+ID766b:-:MIFB1upuECeSPQ8Qj:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(135).translation = [0.31526294100000002 0.56640020400000002 -0.63957018700000001];  % m
smiData.RigidTransform(135).angle = 2.7734925708972393;  % rad
smiData.RigidTransform(135).axis = [0.18615678787662607 0.69474659060964694 0.69474659060964705];
smiData.RigidTransform(135).ID = "B[M1DUpUFaKilJRLDZQ:Mg+9CbpCP8+ID766b:-:MYiVmJ1k9sih0Rs/B:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(136).translation = [0.051249999999999997 -0.085000000000000006 1.155452299];  % m
smiData.RigidTransform(136).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(136).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(136).ID = "F[M1DUpUFaKilJRLDZQ:Mg+9CbpCP8+ID766b:-:MYiVmJ1k9sih0Rs/B:MFq36Jmuu27vIup27]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(137).translation = [-7.4974000000000004e-05 -0.89498656200000004 1.625];  % m
smiData.RigidTransform(137).angle = 0;  % rad
smiData.RigidTransform(137).axis = [0 0 0];
smiData.RigidTransform(137).ID = "B[M8WyaXjmrCxgimslg:MBmYfUzDtlwKYHFP7:-:M5HyekA2D7srTVbBZ:MdKDtG0OMp86xOam7]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(138).translation = [0 -0.89256560799999995 2.4923982150000001];  % m
smiData.RigidTransform(138).angle = 0;  % rad
smiData.RigidTransform(138).axis = [0 0 0];
smiData.RigidTransform(138).ID = "F[M8WyaXjmrCxgimslg:MBmYfUzDtlwKYHFP7:-:M5HyekA2D7srTVbBZ:MdKDtG0OMp86xOam7]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(139).translation = [-0.028749268000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(139).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(139).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(139).ID = "B[M5HyekA2D7srTVbBZ:MrtHIKOk4FgCWOAbM:-:M6mYKQ45uMzHf/HeO:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(140).translation = [0.028750732000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(140).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(140).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(140).ID = "F[M5HyekA2D7srTVbBZ:MrtHIKOk4FgCWOAbM:-:M6mYKQ45uMzHf/HeO:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(141).translation = [0.028750732000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(141).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(141).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(141).ID = "B[M5HyekA2D7srTVbBZ:MrtHIKOk4FgCWOAbM:-:MB0/jg/EbduG+dogU:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(142).translation = [0.028750732000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(142).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(142).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(142).ID = "F[M5HyekA2D7srTVbBZ:MrtHIKOk4FgCWOAbM:-:MB0/jg/EbduG+dogU:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(143).translation = [0.77507630699999996 0.44750047199999998 1.625];  % m
smiData.RigidTransform(143).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(143).axis = [0 0 1];
smiData.RigidTransform(143).ID = "B[M8WyaXjmrCxgimslg:MlAyjgkb9VjluoJR4:-:M5QjUULteb+srYC77:MdKDtG0OMp86xOam7]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(144).translation = [0 -0.89256560799999995 2.4650982149999998];  % m
smiData.RigidTransform(144).angle = 0;  % rad
smiData.RigidTransform(144).axis = [0 0 0];
smiData.RigidTransform(144).ID = "F[M8WyaXjmrCxgimslg:MlAyjgkb9VjluoJR4:-:M5QjUULteb+srYC77:MdKDtG0OMp86xOam7]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(145).translation = [0.028750732000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(145).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(145).axis = [0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(145).ID = "B[M5QjUULteb+srYC77:MrtHIKOk4FgCWOAbM:-:MIb+flQx/XevQtr4C:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(146).translation = [0.028750732000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(146).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(146).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(146).ID = "F[M5QjUULteb+srYC77:MrtHIKOk4FgCWOAbM:-:MIb+flQx/XevQtr4C:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(147).translation = [-0.028749268000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(147).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(147).axis = [-0.57735026918962584 0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(147).ID = "B[M5QjUULteb+srYC77:MrtHIKOk4FgCWOAbM:-:MwP2t+ZQ7SN8Npaqe:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(148).translation = [0.051250732 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(148).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(148).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(148).ID = "F[M5QjUULteb+srYC77:MrtHIKOk4FgCWOAbM:-:MwP2t+ZQ7SN8Npaqe:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(149).translation = [-0.77513347099999996 0.44748289699999999 1.625];  % m
smiData.RigidTransform(149).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(149).axis = [-0 -0 -1];
smiData.RigidTransform(149).ID = "B[M8WyaXjmrCxgimslg:MN3OB2LKtQ8ZDOsfK:-:Mle52KEc5P6JKZBsm:MdKDtG0OMp86xOam7]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(150).translation = [0 -0.89256560799999995 2.4650982149999998];  % m
smiData.RigidTransform(150).angle = 0;  % rad
smiData.RigidTransform(150).axis = [0 0 0];
smiData.RigidTransform(150).ID = "F[M8WyaXjmrCxgimslg:MN3OB2LKtQ8ZDOsfK:-:Mle52KEc5P6JKZBsm:MdKDtG0OMp86xOam7]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(151).translation = [-0.028749268000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(151).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(151).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(151).ID = "B[Mle52KEc5P6JKZBsm:MrtHIKOk4FgCWOAbM:-:MIFB1upuECeSPQ8Qj:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(152).translation = [0.028750732000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(152).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(152).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(152).ID = "F[Mle52KEc5P6JKZBsm:MrtHIKOk4FgCWOAbM:-:MIFB1upuECeSPQ8Qj:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(153).translation = [0.028750732000000001 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(153).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(153).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(153).ID = "B[Mle52KEc5P6JKZBsm:MrtHIKOk4FgCWOAbM:-:MYiVmJ1k9sih0Rs/B:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(154).translation = [0.051250732 -0.84245173399999995 2.4461029569999999];  % m
smiData.RigidTransform(154).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(154).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(154).ID = "F[Mle52KEc5P6JKZBsm:MrtHIKOk4FgCWOAbM:-:MYiVmJ1k9sih0Rs/B:Mh0DTzD1xS0HzvUFp]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(155).translation = [-0.79142035799999999 0.39919173000000002 2.1692857139999999];  % m
smiData.RigidTransform(155).angle = 1.8234765819851761;  % rad
smiData.RigidTransform(155).axis = [0.77459666920292258 0.44721359553335266 0.44721359553335266];
smiData.RigidTransform(155).ID = "B[M8WyaXjmrCxgimslg:MYiMtzI78pOMwz1VG:-:Mle52KEc5P6JKZBsm:MPHEf+y70uqd3Uyvt]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(156).translation = [0.049964821174007024 -0.88252488320474676 2.362354125];  % m
smiData.RigidTransform(156).angle = 3.1415926535135807;  % rad
smiData.RigidTransform(156).axis = [3.8106240385360473e-11 0.70710678118654746 0.70710678118654757];
smiData.RigidTransform(156).ID = "F[M8WyaXjmrCxgimslg:MYiMtzI78pOMwz1VG:-:Mle52KEc5P6JKZBsm:MPHEf+y70uqd3Uyvt]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(157).translation = [-0.050000000000000003 -0.88498600000000005 1.6265595070000001];  % m
smiData.RigidTransform(157).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(157).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(157).ID = "B[M8WyaXjmrCxgimslg:MAQb/MFHsDWrGjUU6:-:M5HyekA2D7srTVbBZ:MGFfhZoWIzFWhFV/4]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(158).translation = [-0.049925025999999997 -0.88256504600000008 1.8690147380000002];  % m
smiData.RigidTransform(158).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(158).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(158).ID = "F[M8WyaXjmrCxgimslg:MAQb/MFHsDWrGjUU6:-:M5HyekA2D7srTVbBZ:MGFfhZoWIzFWhFV/4]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(159).translation = [0.050000012000000003 -0.88498600000000005 2.516098215];  % m
smiData.RigidTransform(159).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(159).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(159).ID = "B[M5QjUULteb+srYC77:MPHEf+y70uqd3Uyvt:-:M8WyaXjmrCxgimslg:MtH0/xWlYVWE1LWim]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(160).translation = [0.74351216809829412 0.48701194842170548 2.2549270400000001];  % m
smiData.RigidTransform(160).angle = 2.7734925710068721;  % rad
smiData.RigidTransform(160).axis = [-0.69474659061724542 -0.18615678781991019 -0.69474659061724542];
smiData.RigidTransform(160).ID = "F[M5QjUULteb+srYC77:MPHEf+y70uqd3Uyvt:-:M8WyaXjmrCxgimslg:MtH0/xWlYVWE1LWim]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(161).translation = [0.036466875000000003 0.103486341 0];  % m
smiData.RigidTransform(161).angle = 0;  % rad
smiData.RigidTransform(161).axis = [0 0 0];
smiData.RigidTransform(161).ID = "AssemblyGround[M8WyaXjmrCxgimslg:M4kUdSjaZv4WNDlsm]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(162).translation = [0.13474204300000001 0.93504572699999999 0.379024949];  % m
smiData.RigidTransform(162).angle = 0;  % rad
smiData.RigidTransform(162).axis = [0 0 0];
smiData.RigidTransform(162).ID = "SixDofRigidTransform[MIFB1upuECeSPQ8Qj]";

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(163).translation = [-0.31238252100000002 -0.72875289300000001 -0.60764741200000005];  % m
smiData.RigidTransform(163).angle = 1.0471975514121594;  % rad
smiData.RigidTransform(163).axis = [0 0 1];
smiData.RigidTransform(163).ID = "SixDofRigidTransform[MIFB1upuECeSPQ8Qj:MFq36Jmuu27vIup27]";


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(87).mass = 0.0;
smiData.Solid(87).CoM = [0.0 0.0 0.0];
smiData.Solid(87).MoI = [0.0 0.0 0.0];
smiData.Solid(87).PoI = [0.0 0.0 0.0];
smiData.Solid(87).color = [0.0 0.0 0.0];
smiData.Solid(87).opacity = 0.0;
smiData.Solid(87).ID = "";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.016430903699999999;  % kg
smiData.Solid(1).CoM = [0.039588196999999999 -0.085000000000000006 1.155452299];  % m
smiData.Solid(1).MoI = [5.9299999999999998e-07 6.6700000000000003e-07 6.6700000000000003e-07];  % kg*m^2
smiData.Solid(1).PoI = [0 0 0];  % kg*m^2
smiData.Solid(1).color = [0.301960784 0.301960784 0.301960784];
smiData.Solid(1).opacity = 1.000000000;
smiData.Solid(1).ID = "JFb*:*7508b6bcbe656f3fbf6c7f3b";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 1.28733042;  % kg
smiData.Solid(2).CoM = [0.040000366000000002 -0.46372586700000001 1.8007776280000001];  % m
smiData.Solid(2).MoI = [0.225562604 0.16778241299999999 0.057803361999999997];  % kg*m^2
smiData.Solid(2).PoI = [0.098453851999999994 -9.5000000000000004e-08 5.5999999999999999e-08];  % kg*m^2
smiData.Solid(2).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(2).opacity = 1.000000000;
smiData.Solid(2).ID = "JFX*:*7508b6bcbe656f3fbf6c7f3b";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.080872494200000006;  % kg
smiData.Solid(3).CoM = [0.040000724000000001 -0.83391637100000005 2.431559231];  % m
smiData.Solid(3).MoI = [2.2662000000000001e-05 1.647e-05 8.9369999999999993e-06];  % kg*m^2
smiData.Solid(3).PoI = [6.7440000000000001e-06 -2.7999999999999999e-08 -4.8e-08];  % kg*m^2
smiData.Solid(3).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(3).opacity = 1.000000000;
smiData.Solid(3).ID = "JFT*:*7508b6bcbe656f3fbf6c7f3b";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.016430903699999999;  % kg
smiData.Solid(4).CoM = [0.039588929000000002 -0.84245173399999995 2.4461029569999999];  % m
smiData.Solid(4).MoI = [5.9299999999999998e-07 6.6700000000000003e-07 6.6700000000000003e-07];  % kg*m^2
smiData.Solid(4).PoI = [0 0 0];  % kg*m^2
smiData.Solid(4).color = [0.301960784 0.301960784 0.301960784];
smiData.Solid(4).opacity = 1.000000000;
smiData.Solid(4).ID = "JFP*:*7508b6bcbe656f3fbf6c7f3b";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.080872494200000006;  % kg
smiData.Solid(5).CoM = [0.040000007999999997 -0.093535361999999997 1.169996026];  % m
smiData.Solid(5).MoI = [2.2662000000000001e-05 1.647e-05 8.9369999999999993e-06];  % kg*m^2
smiData.Solid(5).PoI = [6.7440000000000001e-06 -2.7999999999999999e-08 -4.8e-08];  % kg*m^2
smiData.Solid(5).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(5).opacity = 1.000000000;
smiData.Solid(5).ID = "JFf*:*7508b6bcbe656f3fbf6c7f3b";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 0.0029347860199999999;  % kg
smiData.Solid(6).CoM = [0.049999999000000003 -0.88028600000000001 2.48782573];  % m
smiData.Solid(6).MoI = [3.8000000000000003e-08 3.8000000000000003e-08 1.4999999999999999e-08];  % kg*m^2
smiData.Solid(6).PoI = [0 -0 0];  % kg*m^2
smiData.Solid(6).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(6).opacity = 1.000000000;
smiData.Solid(6).ID = "JFX*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 0.0029347860199999999;  % kg
smiData.Solid(7).CoM = [-0.050000001000000002 -0.88028600000000001 2.3878257299999999];  % m
smiData.Solid(7).MoI = [3.8000000000000003e-08 3.8000000000000003e-08 1.4999999999999999e-08];  % kg*m^2
smiData.Solid(7).PoI = [0 -0 0];  % kg*m^2
smiData.Solid(7).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(7).opacity = 1.000000000;
smiData.Solid(7).ID = "JFP*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 0.24694850800000001;  % kg
smiData.Solid(8).CoM = [0.050002880999999999 -0.882934844 2.4160982180000001];  % m
smiData.Solid(8).MoI = [6.2539999999999994e-05 7.9017000000000006e-05 3.6010000000000003e-05];  % kg*m^2
smiData.Solid(8).PoI = [-0 0 1.0000000000000001e-09];  % kg*m^2
smiData.Solid(8).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(8).opacity = 1.000000000;
smiData.Solid(8).ID = "JFL*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(9).mass = 0.59434864899999995;  % kg
smiData.Solid(9).CoM = [7.3499999999999995e-07 -0.86998600000000004 2.4661859640000001];  % m
smiData.Solid(9).MoI = [0.00095005899999999995 0.001899476 0.00095100200000000005];  % kg*m^2
smiData.Solid(9).PoI = [0 0 0];  % kg*m^2
smiData.Solid(9).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(9).opacity = 1.000000000;
smiData.Solid(9).ID = "JFD*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(10).mass = 0.15487841299999999;  % kg
smiData.Solid(10).CoM = [7.3200000000000004e-07 -0.85736959300000004 2.4461029569999999];  % m
smiData.Solid(10).MoI = [3.4567000000000003e-05 9.1255999999999995e-05 8.5969000000000003e-05];  % kg*m^2
smiData.Solid(10).PoI = [0 0 0];  % kg*m^2
smiData.Solid(10).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(10).opacity = 1.000000000;
smiData.Solid(10).ID = "JFv*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(11).mass = 0.0029347860199999999;  % kg
smiData.Solid(11).CoM = [0.049999999000000003 -0.88028600000000001 2.3878257299999999];  % m
smiData.Solid(11).MoI = [3.8000000000000003e-08 3.8000000000000003e-08 1.4999999999999999e-08];  % kg*m^2
smiData.Solid(11).PoI = [0 -0 0];  % kg*m^2
smiData.Solid(11).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(11).opacity = 1.000000000;
smiData.Solid(11).ID = "JFH*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(12).mass = 4.77366687e-05;  % kg
smiData.Solid(12).CoM = [1.8989999999999999e-06 -0.88945050400000003 2.4659963409999999];  % m
smiData.Solid(12).MoI = [1.6000000000000001e-08 1.4999999999999999e-08 1e-08];  % kg*m^2
smiData.Solid(12).PoI = [0 -0 -0];  % kg*m^2
smiData.Solid(12).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(12).opacity = 1.000000000;
smiData.Solid(12).ID = "JFr*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(13).mass = 0.24694850800000001;  % kg
smiData.Solid(13).CoM = [0.050002880999999999 -0.882934844 2.5160982180000002];  % m
smiData.Solid(13).MoI = [6.2539999999999994e-05 7.9017000000000006e-05 3.6010000000000003e-05];  % kg*m^2
smiData.Solid(13).PoI = [-0 0 1.0000000000000001e-09];  % kg*m^2
smiData.Solid(13).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(13).opacity = 1.000000000;
smiData.Solid(13).ID = "JFb*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(14).mass = 0.62551920900000002;  % kg
smiData.Solid(14).CoM = [-0.049997725 -0.88309907399999998 2.4661373169999998];  % m
smiData.Solid(14).MoI = [0.0013884679999999999 0.0014336279999999999 9.1477999999999994e-05];  % kg*m^2
smiData.Solid(14).PoI = [-7.1e-08 0 2.0000000000000001e-09];  % kg*m^2
smiData.Solid(14).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(14).opacity = 1.000000000;
smiData.Solid(14).ID = "JFT*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(15).mass = 0.0029347860199999999;  % kg
smiData.Solid(15).CoM = [-1.0000000000000001e-09 -0.88028600000000001 2.4378257300000001];  % m
smiData.Solid(15).MoI = [3.8000000000000003e-08 3.8000000000000003e-08 1.4999999999999999e-08];  % kg*m^2
smiData.Solid(15).PoI = [0 -0 0];  % kg*m^2
smiData.Solid(15).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(15).opacity = 1.000000000;
smiData.Solid(15).ID = "JFn*:*bad3f3c5ef53d3b78ec9b469";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(16).mass = 0;  % kg
smiData.Solid(16).CoM = [0 0 0];  % m
smiData.Solid(16).MoI = [0 0 0];  % kg*m^2
smiData.Solid(16).PoI = [0 0 0];  % kg*m^2
smiData.Solid(16).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(16).opacity = 1.000000000;
smiData.Solid(16).ID = "KFTD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(17).mass = 0;  % kg
smiData.Solid(17).CoM = [0 0 0];  % m
smiData.Solid(17).MoI = [0 0 0];  % kg*m^2
smiData.Solid(17).PoI = [0 0 0];  % kg*m^2
smiData.Solid(17).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(17).opacity = 1.000000000;
smiData.Solid(17).ID = "JFf*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(18).mass = 0;  % kg
smiData.Solid(18).CoM = [0 0 0];  % m
smiData.Solid(18).MoI = [0 0 0];  % kg*m^2
smiData.Solid(18).PoI = [0 0 0];  % kg*m^2
smiData.Solid(18).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(18).opacity = 1.000000000;
smiData.Solid(18).ID = "KFbD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(19).mass = 0;  % kg
smiData.Solid(19).CoM = [0 0 0];  % m
smiData.Solid(19).MoI = [0 0 0];  % kg*m^2
smiData.Solid(19).PoI = [0 0 0];  % kg*m^2
smiData.Solid(19).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(19).opacity = 1.000000000;
smiData.Solid(19).ID = "JFr*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(20).mass = 0;  % kg
smiData.Solid(20).CoM = [0 0 0];  % m
smiData.Solid(20).MoI = [0 0 0];  % kg*m^2
smiData.Solid(20).PoI = [0 0 0];  % kg*m^2
smiData.Solid(20).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(20).opacity = 1.000000000;
smiData.Solid(20).ID = "KFHC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(21).mass = 0;  % kg
smiData.Solid(21).CoM = [0 0 0];  % m
smiData.Solid(21).MoI = [0 0 0];  % kg*m^2
smiData.Solid(21).PoI = [0 0 0];  % kg*m^2
smiData.Solid(21).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(21).opacity = 1.000000000;
smiData.Solid(21).ID = "JFL*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(22).mass = 0;  % kg
smiData.Solid(22).CoM = [0 0 0];  % m
smiData.Solid(22).MoI = [0 0 0];  % kg*m^2
smiData.Solid(22).PoI = [0 0 0];  % kg*m^2
smiData.Solid(22).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(22).opacity = 1.000000000;
smiData.Solid(22).ID = "KFvB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(23).mass = 0;  % kg
smiData.Solid(23).CoM = [0 0 0];  % m
smiData.Solid(23).MoI = [0 0 0];  % kg*m^2
smiData.Solid(23).PoI = [0 0 0];  % kg*m^2
smiData.Solid(23).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(23).opacity = 1.000000000;
smiData.Solid(23).ID = "JFT*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(24).mass = 0;  % kg
smiData.Solid(24).CoM = [0 0 0];  % m
smiData.Solid(24).MoI = [0 0 0];  % kg*m^2
smiData.Solid(24).PoI = [0 0 0];  % kg*m^2
smiData.Solid(24).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(24).opacity = 1.000000000;
smiData.Solid(24).ID = "KFTC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(25).mass = 0;  % kg
smiData.Solid(25).CoM = [0 0 0];  % m
smiData.Solid(25).MoI = [0 0 0];  % kg*m^2
smiData.Solid(25).PoI = [0 0 0];  % kg*m^2
smiData.Solid(25).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(25).opacity = 1.000000000;
smiData.Solid(25).ID = "KFXC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(26).mass = 0;  % kg
smiData.Solid(26).CoM = [0 0 0];  % m
smiData.Solid(26).MoI = [0 0 0];  % kg*m^2
smiData.Solid(26).PoI = [0 0 0];  % kg*m^2
smiData.Solid(26).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(26).opacity = 1.000000000;
smiData.Solid(26).ID = "KFnB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(27).mass = 0;  % kg
smiData.Solid(27).CoM = [0 0 0];  % m
smiData.Solid(27).MoI = [0 0 0];  % kg*m^2
smiData.Solid(27).PoI = [0 0 0];  % kg*m^2
smiData.Solid(27).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(27).opacity = 1.000000000;
smiData.Solid(27).ID = "KFzC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(28).mass = 0;  % kg
smiData.Solid(28).CoM = [0 0 0];  % m
smiData.Solid(28).MoI = [0 0 0];  % kg*m^2
smiData.Solid(28).PoI = [0 0 0];  % kg*m^2
smiData.Solid(28).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(28).opacity = 1.000000000;
smiData.Solid(28).ID = "KFLD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(29).mass = 0;  % kg
smiData.Solid(29).CoM = [0 0 0];  % m
smiData.Solid(29).MoI = [0 0 0];  % kg*m^2
smiData.Solid(29).PoI = [0 0 0];  % kg*m^2
smiData.Solid(29).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(29).opacity = 1.000000000;
smiData.Solid(29).ID = "JFn*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(30).mass = 0;  % kg
smiData.Solid(30).CoM = [0 0 0];  % m
smiData.Solid(30).MoI = [0 0 0];  % kg*m^2
smiData.Solid(30).PoI = [0 0 0];  % kg*m^2
smiData.Solid(30).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(30).opacity = 1.000000000;
smiData.Solid(30).ID = "KFrB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(31).mass = 0;  % kg
smiData.Solid(31).CoM = [0 0 0];  % m
smiData.Solid(31).MoI = [0 0 0];  % kg*m^2
smiData.Solid(31).PoI = [0 0 0];  % kg*m^2
smiData.Solid(31).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(31).opacity = 1.000000000;
smiData.Solid(31).ID = "KFPD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(32).mass = 0;  % kg
smiData.Solid(32).CoM = [0 0 0];  % m
smiData.Solid(32).MoI = [0 0 0];  % kg*m^2
smiData.Solid(32).PoI = [0 0 0];  % kg*m^2
smiData.Solid(32).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(32).opacity = 1.000000000;
smiData.Solid(32).ID = "JF/*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(33).mass = 0;  % kg
smiData.Solid(33).CoM = [0 0 0];  % m
smiData.Solid(33).MoI = [0 0 0];  % kg*m^2
smiData.Solid(33).PoI = [0 0 0];  % kg*m^2
smiData.Solid(33).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(33).opacity = 1.000000000;
smiData.Solid(33).ID = "KFrD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(34).mass = 0;  % kg
smiData.Solid(34).CoM = [0 0 0];  % m
smiData.Solid(34).MoI = [0 0 0];  % kg*m^2
smiData.Solid(34).PoI = [0 0 0];  % kg*m^2
smiData.Solid(34).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(34).opacity = 1.000000000;
smiData.Solid(34).ID = "KF3C*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(35).mass = 0;  % kg
smiData.Solid(35).CoM = [0 0 0];  % m
smiData.Solid(35).MoI = [0 0 0];  % kg*m^2
smiData.Solid(35).PoI = [0 0 0];  % kg*m^2
smiData.Solid(35).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(35).opacity = 1.000000000;
smiData.Solid(35).ID = "KFDC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(36).mass = 0;  % kg
smiData.Solid(36).CoM = [0 0 0];  % m
smiData.Solid(36).MoI = [0 0 0];  % kg*m^2
smiData.Solid(36).PoI = [0 0 0];  % kg*m^2
smiData.Solid(36).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(36).opacity = 1.000000000;
smiData.Solid(36).ID = "KFzB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(37).mass = 0;  % kg
smiData.Solid(37).CoM = [0 0 0];  % m
smiData.Solid(37).MoI = [0 0 0];  % kg*m^2
smiData.Solid(37).PoI = [0 0 0];  % kg*m^2
smiData.Solid(37).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(37).opacity = 1.000000000;
smiData.Solid(37).ID = "JFP*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(38).mass = 0;  % kg
smiData.Solid(38).CoM = [0 0 0];  % m
smiData.Solid(38).MoI = [0 0 0];  % kg*m^2
smiData.Solid(38).PoI = [0 0 0];  % kg*m^2
smiData.Solid(38).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(38).opacity = 1.000000000;
smiData.Solid(38).ID = "KFDD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(39).mass = 0;  % kg
smiData.Solid(39).CoM = [0 0 0];  % m
smiData.Solid(39).MoI = [0 0 0];  % kg*m^2
smiData.Solid(39).PoI = [0 0 0];  % kg*m^2
smiData.Solid(39).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(39).opacity = 1.000000000;
smiData.Solid(39).ID = "JF7*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(40).mass = 0;  % kg
smiData.Solid(40).CoM = [0 0 0];  % m
smiData.Solid(40).MoI = [0 0 0];  % kg*m^2
smiData.Solid(40).PoI = [0 0 0];  % kg*m^2
smiData.Solid(40).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(40).opacity = 1.000000000;
smiData.Solid(40).ID = "KFjC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(41).mass = 0;  % kg
smiData.Solid(41).CoM = [0 0 0];  % m
smiData.Solid(41).MoI = [0 0 0];  % kg*m^2
smiData.Solid(41).PoI = [0 0 0];  % kg*m^2
smiData.Solid(41).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(41).opacity = 1.000000000;
smiData.Solid(41).ID = "KFjD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(42).mass = 0;  % kg
smiData.Solid(42).CoM = [0 0 0];  % m
smiData.Solid(42).MoI = [0 0 0];  % kg*m^2
smiData.Solid(42).PoI = [0 0 0];  % kg*m^2
smiData.Solid(42).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(42).opacity = 1.000000000;
smiData.Solid(42).ID = "JFj*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(43).mass = 0;  % kg
smiData.Solid(43).CoM = [0 0 0];  % m
smiData.Solid(43).MoI = [0 0 0];  % kg*m^2
smiData.Solid(43).PoI = [0 0 0];  % kg*m^2
smiData.Solid(43).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(43).opacity = 1.000000000;
smiData.Solid(43).ID = "KFfB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(44).mass = 0;  % kg
smiData.Solid(44).CoM = [0 0 0];  % m
smiData.Solid(44).MoI = [0 0 0];  % kg*m^2
smiData.Solid(44).PoI = [0 0 0];  % kg*m^2
smiData.Solid(44).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(44).opacity = 1.000000000;
smiData.Solid(44).ID = "JFz*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(45).mass = 0;  % kg
smiData.Solid(45).CoM = [0 0 0];  % m
smiData.Solid(45).MoI = [0 0 0];  % kg*m^2
smiData.Solid(45).PoI = [0 0 0];  % kg*m^2
smiData.Solid(45).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(45).opacity = 1.000000000;
smiData.Solid(45).ID = "KFHD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(46).mass = 0;  % kg
smiData.Solid(46).CoM = [0 0 0];  % m
smiData.Solid(46).MoI = [0 0 0];  % kg*m^2
smiData.Solid(46).PoI = [0 0 0];  % kg*m^2
smiData.Solid(46).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(46).opacity = 1.000000000;
smiData.Solid(46).ID = "KFXB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(47).mass = 0;  % kg
smiData.Solid(47).CoM = [0 0 0];  % m
smiData.Solid(47).MoI = [0 0 0];  % kg*m^2
smiData.Solid(47).PoI = [0 0 0];  % kg*m^2
smiData.Solid(47).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(47).opacity = 1.000000000;
smiData.Solid(47).ID = "KF7B*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(48).mass = 0;  % kg
smiData.Solid(48).CoM = [0 0 0];  % m
smiData.Solid(48).MoI = [0 0 0];  % kg*m^2
smiData.Solid(48).PoI = [0 0 0];  % kg*m^2
smiData.Solid(48).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(48).opacity = 1.000000000;
smiData.Solid(48).ID = "KFnD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(49).mass = 0;  % kg
smiData.Solid(49).CoM = [0 0 0];  % m
smiData.Solid(49).MoI = [0 0 0];  % kg*m^2
smiData.Solid(49).PoI = [0 0 0];  % kg*m^2
smiData.Solid(49).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(49).opacity = 1.000000000;
smiData.Solid(49).ID = "JFb*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(50).mass = 0;  % kg
smiData.Solid(50).CoM = [0 0 0];  % m
smiData.Solid(50).MoI = [0 0 0];  % kg*m^2
smiData.Solid(50).PoI = [0 0 0];  % kg*m^2
smiData.Solid(50).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(50).opacity = 1.000000000;
smiData.Solid(50).ID = "KF3B*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(51).mass = 0;  % kg
smiData.Solid(51).CoM = [0 0 0];  % m
smiData.Solid(51).MoI = [0 0 0];  % kg*m^2
smiData.Solid(51).PoI = [0 0 0];  % kg*m^2
smiData.Solid(51).color = [0.917647059 0.917647059 0.917647059];
smiData.Solid(51).opacity = 1.000000000;
smiData.Solid(51).ID = "JOD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(52).mass = 0;  % kg
smiData.Solid(52).CoM = [0 0 0];  % m
smiData.Solid(52).MoI = [0 0 0];  % kg*m^2
smiData.Solid(52).PoI = [0 0 0];  % kg*m^2
smiData.Solid(52).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(52).opacity = 1.000000000;
smiData.Solid(52).ID = "KFfD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(53).mass = 0;  % kg
smiData.Solid(53).CoM = [0 0 0];  % m
smiData.Solid(53).MoI = [0 0 0];  % kg*m^2
smiData.Solid(53).PoI = [0 0 0];  % kg*m^2
smiData.Solid(53).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(53).opacity = 1.000000000;
smiData.Solid(53).ID = "KFDB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(54).mass = 0;  % kg
smiData.Solid(54).CoM = [0 0 0];  % m
smiData.Solid(54).MoI = [0 0 0];  % kg*m^2
smiData.Solid(54).PoI = [0 0 0];  % kg*m^2
smiData.Solid(54).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(54).opacity = 1.000000000;
smiData.Solid(54).ID = "KFLC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(55).mass = 0;  % kg
smiData.Solid(55).CoM = [0 0 0];  % m
smiData.Solid(55).MoI = [0 0 0];  % kg*m^2
smiData.Solid(55).PoI = [0 0 0];  % kg*m^2
smiData.Solid(55).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(55).opacity = 1.000000000;
smiData.Solid(55).ID = "KFzD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(56).mass = 0;  % kg
smiData.Solid(56).CoM = [0 0 0];  % m
smiData.Solid(56).MoI = [0 0 0];  % kg*m^2
smiData.Solid(56).PoI = [0 0 0];  % kg*m^2
smiData.Solid(56).color = [0.231372549 0.380392157 0.705882353];
smiData.Solid(56).opacity = 1.000000000;
smiData.Solid(56).ID = "JMD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(57).mass = 0;  % kg
smiData.Solid(57).CoM = [0 0 0];  % m
smiData.Solid(57).MoI = [0 0 0];  % kg*m^2
smiData.Solid(57).PoI = [0 0 0];  % kg*m^2
smiData.Solid(57).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(57).opacity = 1.000000000;
smiData.Solid(57).ID = "KFnC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(58).mass = 0;  % kg
smiData.Solid(58).CoM = [0 0 0];  % m
smiData.Solid(58).MoI = [0 0 0];  % kg*m^2
smiData.Solid(58).PoI = [0 0 0];  % kg*m^2
smiData.Solid(58).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(58).opacity = 1.000000000;
smiData.Solid(58).ID = "JF3*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(59).mass = 0;  % kg
smiData.Solid(59).CoM = [0 0 0];  % m
smiData.Solid(59).MoI = [0 0 0];  % kg*m^2
smiData.Solid(59).PoI = [0 0 0];  % kg*m^2
smiData.Solid(59).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(59).opacity = 1.000000000;
smiData.Solid(59).ID = "JFD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(60).mass = 0;  % kg
smiData.Solid(60).CoM = [0 0 0];  % m
smiData.Solid(60).MoI = [0 0 0];  % kg*m^2
smiData.Solid(60).PoI = [0 0 0];  % kg*m^2
smiData.Solid(60).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(60).opacity = 1.000000000;
smiData.Solid(60).ID = "KFbC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(61).mass = 0;  % kg
smiData.Solid(61).CoM = [0 0 0];  % m
smiData.Solid(61).MoI = [0 0 0];  % kg*m^2
smiData.Solid(61).PoI = [0 0 0];  % kg*m^2
smiData.Solid(61).color = [0.647058824 0.647058824 0.647058824];
smiData.Solid(61).opacity = 1.000000000;
smiData.Solid(61).ID = "JID*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(62).mass = 0;  % kg
smiData.Solid(62).CoM = [0 0 0];  % m
smiData.Solid(62).MoI = [0 0 0];  % kg*m^2
smiData.Solid(62).PoI = [0 0 0];  % kg*m^2
smiData.Solid(62).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(62).opacity = 1.000000000;
smiData.Solid(62).ID = "KFLB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(63).mass = 0;  % kg
smiData.Solid(63).CoM = [0 0 0];  % m
smiData.Solid(63).MoI = [0 0 0];  % kg*m^2
smiData.Solid(63).PoI = [0 0 0];  % kg*m^2
smiData.Solid(63).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(63).opacity = 1.000000000;
smiData.Solid(63).ID = "KFTB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(64).mass = 0;  % kg
smiData.Solid(64).CoM = [0 0 0];  % m
smiData.Solid(64).MoI = [0 0 0];  % kg*m^2
smiData.Solid(64).PoI = [0 0 0];  % kg*m^2
smiData.Solid(64).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(64).opacity = 1.000000000;
smiData.Solid(64).ID = "JFH*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(65).mass = 0;  % kg
smiData.Solid(65).CoM = [0 0 0];  % m
smiData.Solid(65).MoI = [0 0 0];  % kg*m^2
smiData.Solid(65).PoI = [0 0 0];  % kg*m^2
smiData.Solid(65).color = [0.796078431 0.796078431 0.796078431];
smiData.Solid(65).opacity = 1.000000000;
smiData.Solid(65).ID = "KFPB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(66).mass = 0;  % kg
smiData.Solid(66).CoM = [0 0 0];  % m
smiData.Solid(66).MoI = [0 0 0];  % kg*m^2
smiData.Solid(66).PoI = [0 0 0];  % kg*m^2
smiData.Solid(66).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(66).opacity = 1.000000000;
smiData.Solid(66).ID = "JFX*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(67).mass = 0;  % kg
smiData.Solid(67).CoM = [0 0 0];  % m
smiData.Solid(67).MoI = [0 0 0];  % kg*m^2
smiData.Solid(67).PoI = [0 0 0];  % kg*m^2
smiData.Solid(67).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(67).opacity = 1.000000000;
smiData.Solid(67).ID = "KFfC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(68).mass = 0;  % kg
smiData.Solid(68).CoM = [0 0 0];  % m
smiData.Solid(68).MoI = [0 0 0];  % kg*m^2
smiData.Solid(68).PoI = [0 0 0];  % kg*m^2
smiData.Solid(68).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(68).opacity = 1.000000000;
smiData.Solid(68).ID = "KF7C*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(69).mass = 0;  % kg
smiData.Solid(69).CoM = [0 0 0];  % m
smiData.Solid(69).MoI = [0 0 0];  % kg*m^2
smiData.Solid(69).PoI = [0 0 0];  % kg*m^2
smiData.Solid(69).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(69).opacity = 1.000000000;
smiData.Solid(69).ID = "KFbB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(70).mass = 0;  % kg
smiData.Solid(70).CoM = [0 0 0];  % m
smiData.Solid(70).MoI = [0 0 0];  % kg*m^2
smiData.Solid(70).PoI = [0 0 0];  % kg*m^2
smiData.Solid(70).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(70).opacity = 1.000000000;
smiData.Solid(70).ID = "JFv*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(71).mass = 0;  % kg
smiData.Solid(71).CoM = [0 0 0];  % m
smiData.Solid(71).MoI = [0 0 0];  % kg*m^2
smiData.Solid(71).PoI = [0 0 0];  % kg*m^2
smiData.Solid(71).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(71).opacity = 1.000000000;
smiData.Solid(71).ID = "KFjB*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(72).mass = 0;  % kg
smiData.Solid(72).CoM = [0 0 0];  % m
smiData.Solid(72).MoI = [0 0 0];  % kg*m^2
smiData.Solid(72).PoI = [0 0 0];  % kg*m^2
smiData.Solid(72).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(72).opacity = 1.000000000;
smiData.Solid(72).ID = "KF/C*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(73).mass = 0;  % kg
smiData.Solid(73).CoM = [0 0 0];  % m
smiData.Solid(73).MoI = [0 0 0];  % kg*m^2
smiData.Solid(73).PoI = [0 0 0];  % kg*m^2
smiData.Solid(73).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(73).opacity = 1.000000000;
smiData.Solid(73).ID = "KF/B*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(74).mass = 0;  % kg
smiData.Solid(74).CoM = [0 0 0];  % m
smiData.Solid(74).MoI = [0 0 0];  % kg*m^2
smiData.Solid(74).PoI = [0 0 0];  % kg*m^2
smiData.Solid(74).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(74).opacity = 1.000000000;
smiData.Solid(74).ID = "KFrC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(75).mass = 0;  % kg
smiData.Solid(75).CoM = [0 0 0];  % m
smiData.Solid(75).MoI = [0 0 0];  % kg*m^2
smiData.Solid(75).PoI = [0 0 0];  % kg*m^2
smiData.Solid(75).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(75).opacity = 1.000000000;
smiData.Solid(75).ID = "KFvD*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(76).mass = 0;  % kg
smiData.Solid(76).CoM = [0 0 0];  % m
smiData.Solid(76).MoI = [0 0 0];  % kg*m^2
smiData.Solid(76).PoI = [0 0 0];  % kg*m^2
smiData.Solid(76).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(76).opacity = 1.000000000;
smiData.Solid(76).ID = "KFvC*:*d3d735e5daf49bd8b279af88";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(77).mass = 0.25379101900000001;  % kg
smiData.Solid(77).CoM = [0.40324447200000002 0.54879835600000004 -0.65021548799999995];  % m
smiData.Solid(77).MoI = [0.001359241 0.001359224 5.2553999999999999e-05];  % kg*m^2
smiData.Solid(77).PoI = [1.1328999999999999e-05 2.2399999999999999e-07 -1.2e-08];  % kg*m^2
smiData.Solid(77).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(77).opacity = 1.000000000;
smiData.Solid(77).ID = "JFf*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(78).mass = 0.84984647800000002;  % kg
smiData.Solid(78).CoM = [0.40325002700000001 0.54805065399999997 -0.64486887100000001];  % m
smiData.Solid(78).MoI = [0.0048309429999999999 0.004852571 0.0012826300000000001];  % kg*m^2
smiData.Solid(78).PoI = [6.3411000000000007e-05 2.0000000000000001e-09 3e-09];  % kg*m^2
smiData.Solid(78).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(78).opacity = 1.000000000;
smiData.Solid(78).ID = "JFr*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(79).mass = 0.089692164899999996;  % kg
smiData.Solid(79).CoM = [0.4032501 0.54879843399999995 -0.77534988299999996];  % m
smiData.Solid(79).MoI = [6.1192999999999995e-05 6.1192999999999995e-05 0.00011438900000000001];  % kg*m^2
smiData.Solid(79).PoI = [0 0 0];  % kg*m^2
smiData.Solid(79).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(79).opacity = 1.000000000;
smiData.Solid(79).ID = "JFT*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(80).mass = 0.0162055675;  % kg
smiData.Solid(80).CoM = [0.4032501 0.54879843399999995 -0.49274696000000001];  % m
smiData.Solid(80).MoI = [2.7159999999999999e-06 5.0180000000000001e-06 6.2009999999999996e-06];  % kg*m^2
smiData.Solid(80).PoI = [0 0 0];  % kg*m^2
smiData.Solid(80).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(80).opacity = 1.000000000;
smiData.Solid(80).ID = "JFn*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(81).mass = 0.053270282199999998;  % kg
smiData.Solid(81).CoM = [0.47686225900000001 0.59129843400000004 -0.65448804599999999];  % m
smiData.Solid(81).MoI = [2.5148999999999999e-05 1.6308999999999999e-05 3.1387000000000003e-05];  % kg*m^2
smiData.Solid(81).PoI = [0 0 7.6550000000000004e-06];  % kg*m^2
smiData.Solid(81).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(81).opacity = 1.000000000;
smiData.Solid(81).ID = "JFP*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(82).mass = 0.053270282199999998;  % kg
smiData.Solid(82).CoM = [0.32963794099999999 0.59129843400000004 -0.65448804599999999];  % m
smiData.Solid(82).MoI = [2.5148999999999999e-05 1.6308999999999999e-05 3.1387000000000003e-05];  % kg*m^2
smiData.Solid(82).PoI = [0 0 -7.6550000000000004e-06];  % kg*m^2
smiData.Solid(82).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(82).opacity = 1.000000000;
smiData.Solid(82).ID = "JFL*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(83).mass = 0.039019936999999998;  % kg
smiData.Solid(83).CoM = [0.4032501 0.52227968400000002 -0.76510445299999996];  % m
smiData.Solid(83).MoI = [1.031e-05 5.0838999999999999e-05 6.0012000000000001e-05];  % kg*m^2
smiData.Solid(83).PoI = [0 0 0];  % kg*m^2
smiData.Solid(83).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(83).opacity = 1.000000000;
smiData.Solid(83).ID = "JFb*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(84).mass = 0.039019936999999998;  % kg
smiData.Solid(84).CoM = [0.4032501 0.57531718399999998 -0.76510445299999996];  % m
smiData.Solid(84).MoI = [1.031e-05 5.0838999999999999e-05 6.0012000000000001e-05];  % kg*m^2
smiData.Solid(84).PoI = [0 0 0];  % kg*m^2
smiData.Solid(84).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(84).opacity = 1.000000000;
smiData.Solid(84).ID = "JFX*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(85).mass = 0.0266004933;  % kg
smiData.Solid(85).CoM = [0.4032501 0.54879843399999995 -0.51959424899999995];  % m
smiData.Solid(85).MoI = [7.7619999999999995e-06 7.7619999999999995e-06 1.366e-05];  % kg*m^2
smiData.Solid(85).PoI = [0 0 0];  % kg*m^2
smiData.Solid(85).color = [0.627450980 0.627450980 0.627450980];
smiData.Solid(85).opacity = 1.000000000;
smiData.Solid(85).ID = "JFj*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(86).mass = 0.053270282199999998;  % kg
smiData.Solid(86).CoM = [0.4032501 0.46379843399999998 -0.65448804599999999];  % m
smiData.Solid(86).MoI = [1.1888999999999999e-05 2.9569000000000001e-05 3.1387000000000003e-05];  % kg*m^2
smiData.Solid(86).PoI = [0 0 0];  % kg*m^2
smiData.Solid(86).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(86).opacity = 1.000000000;
smiData.Solid(86).ID = "JFH*:*36be90b51317a6ec062a31eb";

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(87).mass = 0.26690679299999998;  % kg
smiData.Solid(87).CoM = [0.4032501 0.54879843399999995 -0.66760445300000004];  % m
smiData.Solid(87).MoI = [0.00072855999999999995 0.00072855999999999995 0.0014560090000000001];  % kg*m^2
smiData.Solid(87).PoI = [0 0 0];  % kg*m^2
smiData.Solid(87).color = [0.960784314 0.960784314 0.964705882];
smiData.Solid(87).opacity = 1.000000000;
smiData.Solid(87).ID = "JFD*:*36be90b51317a6ec062a31eb";


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the CylindricalJoint structure array by filling in null values.
smiData.CylindricalJoint(3).Rz.Pos = 0.0;
smiData.CylindricalJoint(3).Pz.Pos = 0.0;
smiData.CylindricalJoint(3).ID = "";

smiData.CylindricalJoint(1).Rz.Pos = 0;  % deg
smiData.CylindricalJoint(1).Pz.Pos = 0.62494298400000003;  % m
smiData.CylindricalJoint(1).ID = "[M8WyaXjmrCxgimslg:MBmYfUzDtlwKYHFP7:-:M5HyekA2D7srTVbBZ:MdKDtG0OMp86xOam7]";

smiData.CylindricalJoint(2).Rz.Pos = 29.999999987649218;  % deg
smiData.CylindricalJoint(2).Pz.Pos = 0.57892703999999995;  % m
smiData.CylindricalJoint(2).ID = "[M8WyaXjmrCxgimslg:MlAyjgkb9VjluoJR4:-:M5QjUULteb+srYC77:MdKDtG0OMp86xOam7]";

smiData.CylindricalJoint(3).Rz.Pos = -29.999999987649218;  % deg
smiData.CylindricalJoint(3).Pz.Pos = 0.64702980399999976;  % m
smiData.CylindricalJoint(3).ID = "[M8WyaXjmrCxgimslg:MN3OB2LKtQ8ZDOsfK:-:Mle52KEc5P6JKZBsm:MdKDtG0OMp86xOam7]";


%Initialize the SphericalJoint structure array by filling in null values.
smiData.SphericalJoint(12).S.Pos.Angle = 0.0;
smiData.SphericalJoint(12).S.Pos.Axis = [0.0 0.0 0.0];
smiData.SphericalJoint(12).ID = "";

smiData.SphericalJoint(1).S.Pos.Angle = 47.265893591012208;  % deg
smiData.SphericalJoint(1).S.Pos.Axis = [-0.21191407036066617 -0.80371768709534996 0.55599487968260608];
smiData.SphericalJoint(1).ID = "[MIFB1upuECeSPQ8Qj:MEmCWrSl4Ub5Gt1oM:-:MIFB1upuECeSPQ8Qj:MFq36Jmuu27vIup27]";

smiData.SphericalJoint(2).S.Pos.Angle = 154.24292666336171;  % deg
smiData.SphericalJoint(2).S.Pos.Axis = [-0.33050498651763716 0.08714335654422449 0.93977257317777818];
smiData.SphericalJoint(2).ID = "[MIFB1upuECeSPQ8Qj:Mh0DTzD1xS0HzvUFp:-:MIFB1upuECeSPQ8Qj:MVMOwACUcn0nCEBsD]";

smiData.SphericalJoint(3).S.Pos.Angle = 40.934787991856098;  % deg
smiData.SphericalJoint(3).S.Pos.Axis = [0.24184336432976533 0.63225480802921064 0.73604731155925462];
smiData.SphericalJoint(3).ID = "[M6mYKQ45uMzHf/HeO:MEmCWrSl4Ub5Gt1oM:-:M6mYKQ45uMzHf/HeO:MFq36Jmuu27vIup27]";

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(4).S.Pos.Angle = 150.1710632990378;  % deg
smiData.SphericalJoint(4).S.Pos.Axis = [0.2287909572530315 -0.087514676247023984 0.96953384640280615];
smiData.SphericalJoint(4).ID = "[M6mYKQ45uMzHf/HeO:Mh0DTzD1xS0HzvUFp:-:M6mYKQ45uMzHf/HeO:MVMOwACUcn0nCEBsD]";

smiData.SphericalJoint(5).S.Pos.Angle = 33.397051602343268;  % deg
smiData.SphericalJoint(5).S.Pos.Axis = [0.0040411453912297789 0.18757105992464132 -0.98224272286572512];
smiData.SphericalJoint(5).ID = "[MwP2t+ZQ7SN8Npaqe:MEmCWrSl4Ub5Gt1oM:-:MwP2t+ZQ7SN8Npaqe:MFq36Jmuu27vIup27]";

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(6).S.Pos.Angle = 147.21288673197134;  % deg
smiData.SphericalJoint(6).S.Pos.Axis = [-0.056179845876096174 0.0012103728865924066 -0.99841993164941045];
smiData.SphericalJoint(6).ID = "[MwP2t+ZQ7SN8Npaqe:Mh0DTzD1xS0HzvUFp:-:MwP2t+ZQ7SN8Npaqe:MVMOwACUcn0nCEBsD]";

smiData.SphericalJoint(7).S.Pos.Angle = 49.237139029153575;  % deg
smiData.SphericalJoint(7).S.Pos.Axis = [-0.17727260190561644 0.73551276469354632 -0.65390778981938191];
smiData.SphericalJoint(7).ID = "[MIb+flQx/XevQtr4C:MEmCWrSl4Ub5Gt1oM:-:MIb+flQx/XevQtr4C:MFq36Jmuu27vIup27]";

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(8).S.Pos.Angle = 148.38550793527253;  % deg
smiData.SphericalJoint(8).S.Pos.Axis = [-0.31843878723414398 -0.076749820085292492 -0.94483130975943153];
smiData.SphericalJoint(8).ID = "[MIb+flQx/XevQtr4C:Mh0DTzD1xS0HzvUFp:-:MIb+flQx/XevQtr4C:MVMOwACUcn0nCEBsD]";

smiData.SphericalJoint(9).S.Pos.Angle = 35.2257004494626;  % deg
smiData.SphericalJoint(9).S.Pos.Axis = [0.048976241656422809 -0.44527859025066108 -0.89405162312787934];
smiData.SphericalJoint(9).ID = "[MB0/jg/EbduG+dogU:MEmCWrSl4Ub5Gt1oM:-:MB0/jg/EbduG+dogU:MFq36Jmuu27vIup27]";

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(10).S.Pos.Angle = 35.2257004494626;  % deg
smiData.SphericalJoint(10).S.Pos.Axis = [-0.048976241656422718 0.44527859025066119 0.89405162312787934];
smiData.SphericalJoint(10).ID = "[MB0/jg/EbduG+dogU:Mh0DTzD1xS0HzvUFp:-:MB0/jg/EbduG+dogU:MVMOwACUcn0nCEBsD]";

smiData.SphericalJoint(11).S.Pos.Angle = 46.276443551323837;  % deg
smiData.SphericalJoint(11).S.Pos.Axis = [-0.21004739650541296 -0.79469340898098839 0.56951073470433677];
smiData.SphericalJoint(11).ID = "[MYiVmJ1k9sih0Rs/B:MEmCWrSl4Ub5Gt1oM:-:MYiVmJ1k9sih0Rs/B:MFq36Jmuu27vIup27]";

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(12).S.Pos.Angle = 154.13657420223259;  % deg
smiData.SphericalJoint(12).S.Pos.Axis = [-0.32040147199550723 0.084686111988086635 0.94348882302836712];
smiData.SphericalJoint(12).ID = "[MYiVmJ1k9sih0Rs/B:Mh0DTzD1xS0HzvUFp:-:MYiVmJ1k9sih0Rs/B:MVMOwACUcn0nCEBsD]";

