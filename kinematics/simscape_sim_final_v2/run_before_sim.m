%% Load the ONshape unconfigured model

%  assemblyURL = 'https://cad.onshape.com/documents/2c6fc39eaa53780054772e05/w/253c56d792bcff6ea43cb14a/e/7018562b3fd7ddf09c498d9c';
%  exportedModel = smexportonshape(assemblyURL);
%  smimport(exportedModel);

%% Load friction Parameters

 beta_linear = 0.02;
 beta_ball = 0.02;


%% Load Actuator Positions for the simulation 
load('benchmark_positions.csv')
time = benchmark_positions(:,4);
act1 = benchmark_positions(:,1);
act2 = benchmark_positions(:,2);
act3 = benchmark_positions(:,3);
act1_norm = 1-act1;
act2_norm = 1-act2;
act3_norm = 1-act3;
pos1= timeseries(act1_norm,time);
pos2= timeseries(act2_norm,time);
pos3= timeseries(act3_norm,time);
save("positions_timeseries_1.mat","pos1","-v7.3");
save("positions_timeseries_2.mat","pos2","-v7.3");
save("positions_timeseries_3.mat","pos3","-v7.3");
% 

%% Extract values From simulink
% 
 Force_motor1 = out.Forces{1}.Values.Data(1,:);
 Force_motor2 = out.Forces{2}.Values.Data(1,:);
 Force_motor3 = out.Forces{3}.Values.Data(1,:);
 t = out.tout;


%% Remove outliers
% Force_motor_timetable1 = [t,Force_motor1'];
% Force_motor_timetable1 = rmoutliers(Force_motor_timetable1,"mean");
% Force_motor_timetable2 = [t,Force_motor2'];
% Force_motor_timetable2 = rmoutliers(Force_motor_timetable2,"mean");
% Force_motor_timetable3 = [t,Force_motor3'];
% Force_motor_timetable3 = rmoutliers(Force_motor_timetable3,"mean");

%% Load normalized forces from workspace
% load('Force_motor_timetable.mat')

%% Fit lines
%Evaluate the polynomial on a finer grid and plot the results.
% x_1 = t;
% y_1 = Force_motor1;
% n = 20;
% p = polyfit(x_1,y_1,n);
% x1 = x_1;
% y1 = polyval(p,x1);
% 
% x_2 = t;
% y_2 = Force_motor2;
% n = 20;
% p = polyfit(x_2,y_2,n);
% x2 = x_2;
% y2 = polyval(p,x2);
% 
% x_3 = t;
% y_3 = Force_motor3;
% n = 20;
% p = polyfit(x_3,y_3,n);
% x3 = x_3;
% y3 = polyval(p,x3);
% 
%% Plot data
figure(1)
plot(t,Force_motor1);
hold on 
%plot(x1,y1,LineWidth=2)
plot(t,Force_motor2);
%plot(x2,y2,LineWidth=2)
plot(t(:,1),Force_motor3);
%plot(x3,y3,LineWidth=2)
%legend("Force Actuator 1","Fitted Line 1","Force Actuator 2","Fitted Line 2","Force Actuator 3", "Fitted Line 3");
legend("Force Actuator 1","Force Actuator 2","Force Actuator 3");
xlabel("time(s)")
ylabel("Force(N)")
ylim([-200,300])


